from pickle import load
from pickle import dump
from datetime import datetime

from Texto.debugging_insights import get_process_status as status_log
from Texto.cleaning import clean_string_list, clean_string

from os import path as os_path
from os import makedirs as os_makedirs
from re import split as re_split

from numpy import isclose, array, int32, ndarray

#Author : Javier D.  
def tokenize_string(input_string, split_chars=" ", keep_split_chars=False):

    """

    Función que obtiene los tokens de un string
    ...
    Parameters
    ----------

    input_string : str
        String a tokenizar.

    split_chars : list of char o str, optional
        Caracteres que se usarán para separar los tokens.

    keep_splitters : bool
        Booleano que indica si en el resultado del tokenizado se deben conservar los caracteres de spliteo.


    Returns
    -------

    tokenized_string : list of str
        Lista de strings que representan los tokens.

    """

    split_expression = '('
    for index, char in enumerate(split_chars):
        
        if (index == len(split_chars) - 1):
            split_expression += "\\" + char + ")"
        else:
            split_expression += "\\" + char + "|"

    if(len(split_chars) > 1 or keep_split_chars):
        tokenized_string = re_split(split_expression, input_string)
        if(keep_split_chars):
            tokenized_string = [x for x in tokenized_string if x]
        else:
            uncleaned_tokenized_string = [clean_string(x, split_chars, True) for x in tokenized_string if x]
            tokenized_string = []
            #La función split por defecto no conserva los vacíos que se encuentren a continuación de un token no vacío("")
            for token_index, token in enumerate(uncleaned_tokenized_string):
                if( (token_index > 0 and token == "" and uncleaned_tokenized_string[token_index-1]=="") or token!= ""):
                    tokenized_string.append(token)
                elif (token_index == 0 and token == ""):
                    tokenized_string.append(token)        
    else:
        #Splittear con un solo caracter sin conservar el mismo en el resultado es más rapido con la función split de los strings
        tokenized_string = input_string.split(split_chars)

    return tokenized_string

def tokenize_string_list(input_string_list, split_chars=" ", keep_split_chars=False,
                        print_status = False, status_verbose = 0):

    """
    Función que obtiene los tokens de una lista de strings a través de splits.

    Parameters
    ----------
    input_string_list : list of str
        Lista de strings a tokenizar

    split_chars : char, optional
        Caracteres que se usarán para separar los tokens.

    keep_splitters : bool
        Booleano que indica si en el resultado del tokenizado se deben conservar los caracteres de spliteo.

    print_status : bool, optional
        Booleano para determinar si se desea un feedback del status de procesamiento de la lista dada (por defecto True)

    status_verbose : int, optional
        Modo de impresión del feedback, verbose = 0 corresponde a un mensaje genérico de Loading estático que imprime un mensaje 
        en la primer y último ciclo, verbose > 0 imprime una barra de progreso en cada ciclo (Por defecto es 0).

    Returns
    -------
    tokenized_sentenceList : list of list of str
        Lista de strings o tokens de las oraciones

    """

    tokenized_strings_list = []
    total_data = len(input_string_list)

    for p_index, string in enumerate(input_string_list):

        string_tokens = tokenize_string(string, split_chars, keep_split_chars)
        tokenized_strings_list.append(string_tokens)

        if (print_status):
            status_log(p_index, total_data, "Getting string tokens",increment_percentaje=20,memory_warnings=False)

    return tokenized_strings_list

def get_lines_from_text_file(file_path):
    """
    Función que lee un archivo de texto y devuelve una lista con los strings de las 
    lineas de dicho archivo.

    Parameters
    ----------
    file_path: str 
        Ruta del archivo a leer.

    Returns
    -------
    file_lines : list of list of str, or dict
        Lista con las lineas del archivo.  

    """

    with open(file_path,"r") as file:
        file_lines = []
        for line in file:
            line = line.replace("\n"," ")
            file_lines.append(line)
    
    return file_lines

def concatenate_lists(lists, as_string=False, print_status = False, status_verbose=0):
    """
    Función que recibe una lista de listas y devuelve un string o lista que representa la unión de todas ellas 
    como un gran texto o lista.

    Parameters
    ----------
    lists : list of object
        Lista de listas de objetos.

    as_string : bool, optional
        Booleano que indica si el texto devuelto es una listas de palabras o un string(Por defectos es False).

    print_status : bool, optional
        Booleano para determinar si se desea un feedback del status de procesamiento de la lista dada (por defecto True).

    status_verbose : int, optional
        Modo de impresión del feedback, verbose = 0 corresponde a un mensaje genérico de Loading estático que imprime un mensaje 
        en la primer y último ciclo, verbose > 0 imprime una barra de progreso en cada ciclo (Por defecto es 0).

    Returns
    -------
    result : list of object or str
        Texto generado al unir todas las lineas.

        
    """

    if (as_string):
        result = ""

    else:
        result = []

    total_data = len(lists)
    for p_index, a_list in enumerate(lists):

        for element in a_list:
            if (as_string):
                result += element + " "

            else:
                result.append(element)

        if (print_status):
            status_log(p_index, total_data, "Parsing Separated Text Lines to a Single Text",increment_percentaje=20,memory_warnings=False)
    
    return result

def find_elements_max_lenght(a_list):  
    """
    Función que recibe una lista de objetos y devuelve el largo del elemento más largo

    Parameters
    ----------
    a_list : list of object
        Lista con elementos

    Returns
    -------

    max_len_element : object
        Elemento que tiene el largo del elemento más grande.

    max_len : int
        Largo del elemento más grande.
        
    """
    max_len_element = max(a_list, key=len)
    max_len = len(max_len_element)

    return max_len_element, max_len

def find_elements_min_lenght(a_list):
     
    """
    Función que recibe una lista de objetos y devuelve el largo del elemento más pequeño
    
    Parameters
    ----------
    a_list : list of object
        Lista con elementos.

    Returns
    -------
    min_len_element : object
        Elemento que tiene el largo del elemento más pequeño.

    min_len : int
        Largo del elemento más pequeño.

        
    """
    min_len_element = min(a_list, key=len)
    min_len = len(min_len_element)

    return min_len_element, min_len

def pad(input_list, pad_until_size = 1, pad_element = ''):
     
    """
    Función que paddea una lista.

    Parameters
    ----------
    input_list : list of object
        Lista a paddear

    pad_until_size : int, optional
        Largo que debe tener la lista. El padding total es:  pad_until_size - largo_palabra. (Por defecto es 1).

    pad_element : obj, optional
        Objeto con el que se va a padear la lista (Por defecto es el string vacío ('') ).

    Returns
    -------
    padded_list : list of object
        Lista paddeada.
        
    """
    padded_list = []

    if (len(input_list) > pad_until_size):
        raise NameError('You attempted to pad a list to a size of:' + str(pad_until_size) + 
                " but a list (" + str(input_list) + ") with lenght: " + str (len(input_list)) + 
                                                                " was found, padding is not consistent!") 

    for element in input_list:
        padded_list.append(element)

    while ( len(padded_list) < pad_until_size):
        padded_list.append(pad_element)

    return padded_list

def pad_list_of_lists(input_list, pad_until_size = 0, pad_element = "", 
                        print_status = False, status_verbose = 0):
    
    """
    Función que paddea las listas individuales de una lista de entrada.

    Parameters
    ----------

    input_list : list of list of object
        Lista de listas a paddear

    pad_until_size : int, optional
        Largo que debe tener cada lista de la lista. El padding total es de largo_de_elemento_lista - pad_until_size_de_lista. (Por defecto es 0, que es el largo de la lista más grande de la lista, cualquier número menor o igual a 0 produce el mismo resultado).
    
    pad_element : obj, optional
        Objeto con el que se va a padear la lista (Por defecto es el string vacío ('') ).

    print_status : bool, optional
        Booleano para determinar si se desea un feedback del status de procesamiento de la lista dada (por defecto True)

    status_verbose : int, optional
        Modo de impresión del feedback, verbose = 0 corresponde a un mensaje genérico de Loading estático que imprime un mensaje 
        en la primer y último ciclo, verbose > 0 imprime una barra de progreso en cada ciclo (Por defecto es 0).

    Returns
    -------
    padded_sentenceList : list of list of object
        Lista de objetos paddeados.

        
    """
    
    padded_list = []

    if (pad_until_size <= 0):
        _, pad_until_size = find_elements_max_lenght(input_list)

    total_data = len(input_list)
    for p_index, sentence in enumerate(input_list):

        padded_element = pad(sentence, pad_until_size, pad_element)
        padded_list.append(padded_element)

        if (print_status):
            status_log(p_index, total_data, "Padding Sentences List",increment_percentaje=20,memory_warnings=False)

    return padded_list

def pad_sentence_list_ngrams(sentence_list_ngrams, pad_until_size = 0, pad_element = "",
                            print_status = False, status_verbose = 0):
    
    """
    Función especial que paddea una lista de ngramas de entrada que representan una palabra. 

    Parameters
    ----------

    sentence_list_ngrams : list of list of list of str
        Lista de oraciones de ngramas a paddear

    pad_until_size : int, optional
        Largo de ngramas que debe tener una oración. El padding total es de pad_until_size - largo_oración (Por defecto es 0, que es el largo de la lista más grande de la lista, cualquier número menor o igual a 0 produce el mismo resultado).

    pad_element : char, optional
        Caracter con el que se va a padear la oración (Por defecto es vacío "" ).

    print_status : bool, optional
        Booleano para determinar si se desea un feedback del status de procesamiento de la lista dada (por defecto True)

    status_verbose : int, optional
        Modo de impresión del feedback, verbose = 0 corresponde a un mensaje genérico de Loading estático que imprime un mensaje 
        en la primer y último ciclo, verbose > 0 imprime una barra de progreso en cada ciclo (Por defecto es 0).

    Returns
    -------
    padded_sentence_list_ngrams : list of list of list of str
        Lista de oraciones de ngramas paddeadas.

        
    """
    
    padded_sentence_list_ngrams = []

    if (pad_until_size <= 0):

        max_len = -99999
        for sentence in sentence_list_ngrams:
            _, pad_until_size = find_elements_max_lenght(sentence)

            if (pad_until_size>max_len):
                max_len = pad_until_size

        pad_until_size = max_len
    
    total_data = len(sentence_list_ngrams)
    for p_index, sentence in enumerate(sentence_list_ngrams):

        padded_sentence = pad_list_of_lists(sentence, pad_until_size, pad_element,
                                            print_status=False)

        padded_sentence_list_ngrams.append(padded_sentence)

        if (print_status):
            status_log(p_index, total_data, "Padding list of words ngrams", increment_percentaje=20,memory_warnings=False)

    return padded_sentence_list_ngrams

def get_ngrams_for_word(word, min_ngram_window=2, max_ngram_window=2, 
                         include_wordgram=True, print_status = False, 
                         status_verbose = 0):
    
    """
    Función que obtiene los ngramas de una palabra en base a una ventana deslizante (sliding window)
    que puede ser en base a una cierta cantidad de caracteres fija o en base a una cantidad minima y otra
    máxima.

    Nota: Este procesamiento agrega delimitadores "< >" a la palabra entregada. 
    Ej: "prueba" será transformada en "<prueba>"

    Parameters
    ----------

    word : str
        Palabra de la cual se obtendran los ngramas.

    min_ngram_window : int, optional
        Dimensión mínima de las ventanas de ngramas a obtener, esto es, la cantidad de caracteres que tendra un ngrama (Por defecto es 2)

    max_ngram_window : int, optional
        Dimensión máxima de las ventanas de ngramas a obtener, esto es, la cantidad de caracteres que tendra un ngrama (Por defecto es 2)

    include_wordgram : bool, optional
        Booleano que indica si se quiere agregar el ngrama palabra dentro de los ngramas obtenidos (Por defecto es True). 
        Ej: si se considera solo ngrama de dos caracteres y tenemos la palabra "prueba", si incluimos el ngrama palabra, 
        los ngramas de la palabra dada serían ["<p,"pa","al","la","ab","br","ra","a>","<palabra>"], en caso contrario, 
        si no se incluyera, sería solo ["<p,"pa","al","la","ab","br","ra","a>"]. El comportamiento estandar de fastText 
        considera esto como verdadero.

    print_status : bool, optional
        Booleano para determinar si se desea un feedback del status de procesamiento de la lista dada (por defecto True)

    status_verbose : int, optional
        Modo de impresión del feedback, verbose = 0 corresponde a un mensaje genérico de Loading estático que imprime un mensaje 
        en la primer y último ciclo, verbose > 0 imprime una barra de progreso en cada ciclo (Por defecto es 0).

    Returns
    -------

    ngrams_of_word : list of str
        Lista de ngramas de la palabra de entrada.

        
    """

    if (min_ngram_window > max_ngram_window):
        raise NameError ("You passed a maximum ngram window of {} when the minimum is {}".format(str(max_ngram_window), str(min_ngram_window)))

    if (min_ngram_window <= 0):
        raise NameError ("You passed a minimum ngram window of {} which is not valid, use values higher than 0".format(str(min_ngram_window)))
    
    minMaxWindowed = False
    
    if (max_ngram_window != min_ngram_window):
        minMaxWindowed = True

    if (word != ""):
        word = "<"+word+">"

    if (include_wordgram):
        ngrams_of_word = [word]
    else:
        ngrams_of_word = []

    if(not minMaxWindowed):
        total_data = len(word)- (min_ngram_window-1)
        for charIndex in range(len(word)- (min_ngram_window-1) ):
            ngram = word[charIndex:charIndex+min_ngram_window]
            ngrams_of_word.append(ngram)
            if (print_status):
                status_log(charIndex, total_data, "Getting Ngrams for Word", increment_percentaje=20,memory_warnings=False)
        return ngrams_of_word
    else:
        total_data = min_ngram_window,max_ngram_window+1
        for ngram_window in range(min_ngram_window,max_ngram_window+1):
            for charIndex in range(len(word)- (ngram_window-1) ):
                ngram = word[charIndex:charIndex+ngram_window]
                ngrams_of_word.append(ngram)
            if (print_status):
                status_log(ngram_window, total_data, "Getting Ngrams for Word", increment_percentaje=20,memory_warnings=False)
    
        return ngrams_of_word

def get_ngrams_for_sentence(sentence, min_ngram_window=2, max_ngram_window=2 , include_wordgram=True, print_status = False,
                            status_verbose = 0):
    
    """
    Función que obtiene los ngramas de una oración y sus palabras en base a una ventana deslizante (sliding window)
    que puede ser en base a una cierta cantidad de caracteres fija o en base a una cantidad minima y otra
    máxima.

    Nota: Este procesamiento agrega delimitadores "< >" a la palabras de cada oración. 
    Ej: "prueba" será transformada en "<prueba>".
    ...

    Parameters
    ----------

    sentence : list of str
        Oración de la cual se obtendran los ngramas.

    min_ngram_window : int, optional
        Dimensión mínima de las ventanas de ngramas a obtener, esto es, la cantidad de caracteres que tendra un ngrama (Por defecto es 2)

    max_ngram_window : int, optional
        Dimensión máxima de las ventanas de ngramas a obtener, esto es, la cantidad de caracteres que tendra un ngrama (Por defecto es 2)

    concatenated : bool, optional
        Booleano que indica si los ngramas obtenidos de cada palabra deben conformar una lista de strings concatenada (la oración 
        como un gran grupo de ngramas) o una lista de listas de string (la oración como un grupo de palabras representadas por ngramas)
        (Por defecto es False).

    minMaxWindowed : bool, optional
        Booleano que indica si se quiere obtener los ngramas de una palabra en base a una cantidad de caracteres minima y maxima, o solo
        ngramas con una cantidad de caracteres fija (toma como valor referencial "min_ngram_window")(Por defecto es False).

    include_wordgram : bool, optional
        Booleano que indica si se quiere agregar el ngrama palabra dentro de los ngramas obtenidos (Por defecto es True). 
        Ej: si se considera solo ngrama de dos caracteres y tenemos la palabra "prueba", si incluimos el ngrama palabra, 
        los ngramas de la palabra dada serían ["<p,"pa","al","la","ab","br","ra","a>","<palabra>"], en caso contrario, 
        si no se incluyera, sería solo ["<p,"pa","al","la","ab","br","ra","a>"]. El comportamiento estandar de fastText 
        considera esto como verdadero.

    print_status : bool, optional
        Booleano para determinar si se desea un feedback del status de procesamiento de la lista dada (Por defecto es True)

    status_verbose : int, optional
        Modo de impresión del feedback, verbose = 0 corresponde a un mensaje genérico de Loading estático que imprime un mensaje 
        en la primer y último ciclo, verbose > 0 imprime una barra de progreso en cada ciclo (Por defecto es 0).

    Returns
    -------
        sentence_as_ngrams : list of list of str or list of str
            Lista de palabras representadas por ngramas de la oración entregada

        
    """
    sentence_as_ngrams=[]
    total_data = len(sentence)
    for wordindex,word in enumerate(sentence):
        sentence_as_ngrams.append(get_ngrams_for_word(word,min_ngram_window, 
                                                        max_ngram_window, include_wordgram,
                                                        print_status=False))
        if (print_status):
            status_log(wordindex, total_data, "Getting Ngrams for Sentence", increment_percentaje=20,memory_warnings=False)

    return sentence_as_ngrams

def get_ngrams_for_sentences_list(sentences_list, min_ngram_window=2, max_ngram_window=2 ,
                                include_wordgram=True, print_status = False,
                                status_verbose=0):
    
    """
    Función que obtiene los ngramas de una lista de oraciones y sus palabras en base a una ventana deslizante (sliding window)
    que puede ser en base a una cierta cantidad de caracteres fija o en base a una cantidad minima y otra
    máxima.

    Nota: Este procesamiento agrega delimitadores "< >" a la palabras de cada oración. 
    Ej: "prueba" será transformada en "<prueba>"

    Parameters
    ----------

    sentences_list : list of list of str
        Lista de oraciones de la cual se obtendran los ngramas.

    min_ngram_window : int, optional
        Dimensión mínima de las ventanas de ngramas a obtener, esto es, la cantidad de caracteres que tendra un ngrama (Por defecto es 2)

    max_ngram_window : int, optional
        Dimensión máxima de las ventanas de ngramas a obtener, esto es, la cantidad de caracteres que tendra un ngrama (Por defecto es 2)

    minMaxWindowed : bool, optional
        Booleano que indica si se quiere obtener los ngramas de una palabra en base a una cantidad de caracteres minima y maxima, o solo
        ngramas con una cantidad de caracteres fija (toma como valor referencial "min_ngram_window")(Por defecto es False).

    include_wordgram : bool, optional
        Booleano que indica si se quiere agregar el ngrama palabra dentro de los ngramas obtenidos (Por defecto es True). 
        Ej: si se considera solo ngrama de dos caracteres y tenemos la palabra "prueba", si incluimos el ngrama palabra, 
        los ngramas de la palabra dada serían ["<p,"pa","al","la","ab","br","ra","a>","<palabra>"], en caso contrario, 
        si no se incluyera, sería solo ["<p,"pa","al","la","ab","br","ra","a>"]. El comportamiento estandar de fastText 
        considera esto como verdadero.

    print_status : bool, optional
        Booleano para determinar si se desea un feedback del status de procesamiento de la lista dada (Por defecto es True)

    status_verbose : int, optional
        Modo de impresión del feedback, verbose = 0 corresponde a un mensaje genérico de Loading estático que imprime un mensaje 
        en la primer y último ciclo, verbose > 0 imprime una barra de progreso en cada ciclo (Por defecto es 0).

    Returns
    -------
    sentencesList_as_ngrams : list of list of list of str or list of list of str
        Lista de oraciones representadas por ngramas de la oraciones entregadas

        
    """
    sentences_list_as_ngrams=[]
    total_data = len(sentences_list)
    for p_index, WordList in enumerate(sentences_list):
        sentences_list_as_ngrams.append(get_ngrams_for_sentence(WordList, min_ngram_window, 
                                                            max_ngram_window, include_wordgram, 
                                                            print_status = False))
        if (print_status):
            status_log(p_index, total_data, "Getting Ngrams for Sentence List", increment_percentaje=20,memory_warnings=False)
    return sentences_list_as_ngrams

def load_pickle(pickle_loc):

    """
    Función que carga un pickle

    ----------
    pickle_loc : string
        Ruta del pickle a cargar.

    Returns
    -------
    obj : object
        Pickle como objeto

    """
    
    if (pickle_loc.split(".")[-1]!="pkl"):
        raise NameError("Seems like you tried to open a wrong file or format" +
                " File Name: {} , needs to be a valid pickle (.pkl) file".format(pickle_loc))
    with open(pickle_loc,"rb") as pickle_in:
        obj = load(pickle_in)

    
    return obj

def save_pickle(object_to_save, pickle_name = "default", saving_folder_path="./"):

    """
    Función que carga un pickle

    ----------

    object_to_save : object
        Objeto que se quiere almacenar como pickle.

    pickle_name : str
        Nombre del archivo pickle a guardar (sin el .pkl).

    saving_folder_path : str
        Ruta en donde se desea guardar el archivo pickle.
            
    Returns
    -------
    pickle_path : str
        Ruta del nuevo archivo pickle

    """
    
    if (pickle_name == "default"):
        
        dateTimeObj = datetime.now()

        pickle_name = "pickle_" + str(dateTimeObj.day) + '_'+ str(dateTimeObj.month) + '_' + str(dateTimeObj.year) + '_' + \
								 str(dateTimeObj.hour) + '_'+ str(dateTimeObj.minute) + '_' + str(dateTimeObj.second) + '_' +  str(dateTimeObj.microsecond)
         
    if not os_path.exists(saving_folder_path):
        os_makedirs(saving_folder_path)

    pickle_path = saving_folder_path + pickle_name + ".pkl"
    
    with open(pickle_path, "wb") as pickle_file:
        dump(object_to_save,pickle_file)

    return pickle_path

def pad_string_lists_for_tabulation(list_of_string_rows, print_status=False, status_verbose=0):

    """
    Función que realiza padding sobre una lista de listas de strings en un formato de pseudo tabla, 
    donde cada lista de strings de la lista maestra representará una FILA de la tabla, y cada uno 
    de los elementos de cada sub-lista es una columna, por ende todas las listas deben tener igual largo.

    El espacio y tamaño en ancho de cada COLUMNA se calcula según el string más largo entra las columnas de todas las filas.

    Esta función es útil para realizar prints alineados en la consola.

    ----------

    list_of_string_rows : list of lists of string
        Lista de listas de string que se desean paddear
            
    Returns
    -------
    tabulated_list_of_lists_of_string : list of lists of string
        Lista de listas de string ya paddeadas, donde cada lista representa una FILA lista para ser impresa.

    """
    _, columns = find_elements_max_lenght(list_of_string_rows)

    tabulated_columns = []
    input_columns = []
    for x in range(columns):
         input_columns.append([])

    rows = pad_list_of_lists(list_of_string_rows, columns, "")

    for row in rows:
        for column_index, column in enumerate(row):
            input_columns[column_index].append(column)
    for col_num, string_list in enumerate(input_columns):
        
        tabulated_column = []   
        _, max_len = find_elements_max_lenght(string_list)
        
        for element in string_list:

            element_length = len(element)
            length_difference_with_max_len = max_len - element_length
            
            if(length_difference_with_max_len>0):
                element += " "*length_difference_with_max_len

            tabulated_column.append(element)
        tabulated_columns.append(tabulated_column)
        if (print_status):
            status_log(col_num, columns, "Tabulating columns", increment_percentaje=20,memory_warnings=False)

    tabulated_rows = []
    for x in range(len(tabulated_columns[0])):
        tabulated_rows.append("|")

    for column in tabulated_columns:
        for row_index in range(len(column)):
            tabulated_rows[row_index] += column[row_index] + "|"

    return tabulated_rows

def get_padding_mask_for_sequence(sequence, padding_element = "", token_type = "string",
                                    print_status = False, status_verbose = 0):

    """
    Función que genera una lista 'logíca' de enmáscaramiento para una secuencia, donde se coloca un valor por
    cada elemento de la lista (1 = debe enmascararse, 0 no debe enmascararse)

    Esta función fue pensada para crear capas customizadas de redes neuronales que anulen la conexión o no
    propaguen la información de aquellos tokens que representan "padding".

    El token de padding se puede especificar como string o nparray, en cualquiera de los dos casos la secuencia
    a enmáscarar debe estar o cumplir que son del mismo tipo. 

    ----------

    sequence : list of string or list of nparray
        Secuencia a la que se le desea obtener su máscara.

    token_type : str, optional
        Tipo de datos de la secuencia, string o nparray (Por defecto es "string")
            
    Returns
    -------
    sequence_mask : list of int
        Lista de enteros con los valores de enmáscaramiento de cada elemento de la secuencia de entrada

    """

    allowed_token_types = ["string", "nparray"]

    assert (token_type in allowed_token_types),\
            "The chosen token type ({}) is not valid, options are {}".format(token_type, str(allowed_token_types))

    if(token_type == "string"):

        assert isinstance(padding_element, str),\
             "The padding token passed ({}) is not a string".format(padding_element, str(allowed_token_types))

        assert isinstance(sequence,list) and all(isinstance(x,str) for x in sequence),\
             "The sequence passed is either not a list or its elements are not strings"

    if(token_type == "nparray"):

        assert (type(padding_element) is ndarray),\
             "The padding token passed ({}) is not a nparray".format(padding_element, str(allowed_token_types))

        assert isinstance(sequence,list) and all((type(padding_element) is ndarray) for x in sequence),\
             "The sequence passed is either not a list or its elements are not nparrays"

    if(token_type == "string"):

        string_nparray = array(sequence)
        mask = (string_nparray == array(padding_element))

    if(token_type == "nparray"):
        mask = []
        for element in sequence:
            mask.append((element == padding_element).all())

    sequence_mask = int32(mask).tolist()
    return sequence_mask

def get_padding_mask_for_sequences_list(sequences_list, padding_element = "", token_type = "string",
                                    print_status = False, status_verbose = 0):

    """
    Función que genera una lista 'logíca' de enmáscaramiento para una secuencia, donde se coloca un valor por
    cada elemento de cada sub-lista de la lista de secuencias de entrada (1 = debe enmascararse, 0 no debe enmascararse)

    Esta función fue pensada para crear capas customizadas de redes neuronales que anulen la conexión o no
    propaguen la información de aquellos tokens que representan "padding".

    El token de padding se puede especificar como string o nparray, en cualquiera de los dos casos la secuencia
    a enmáscarar debe estar o cumplir que son del mismo tipo. 

    ----------

    sequences_list : list of list of string or list of list of nparray
        Secuencias a la que se le desea obtener sus máscaras.
            
    token_type : str, optional
        Tipo de datos de las secuencias, string o nparray (Por defecto es "string")

    Returns
    -------
    sequences_masks : list of list of int
        Lista de listas de enteros con los valores de enmáscaramiento de cada elemento de la secuencias de entrada

    """

    sequences_masks = []

    total_data = len(sequences_list)
    for p_index, sequence in enumerate(sequences_list):
         
        if(print_status):
            status_log(p_index, total_data, "Getting padding masks for sequence list", increment_percentaje=20,memory_warnings=False)

        token_mask = get_padding_mask_for_sequence(sequence, padding_element, token_type)
        sequences_masks.append(token_mask)

    return sequences_masks

    

