# uncompyle6 version 3.6.3
# Python bytecode 3.6 (3379)
# Decompiled from: Python 3.6.8 (default, Oct  7 2019, 12:59:55) 
# [GCC 8.3.0]
# Embedded file name: ../nlp_utils/embedding.py
# Compiled at: 2020-01-28 17:01:01
# Size of source mod 2**32: 48149 bytes

import numpy as np
import logging as log

from Texto.debugging_insights import get_process_status as status_log
from Texto.common import get_ngrams_for_sentences_list, get_ngrams_for_sentence, get_ngrams_for_word, pad_sentence_list_ngrams, save_pickle, load_pickle
from Texto.vocabulary import encode_element, encode_list_of_elements, encode_list_of_elements

def generate_Word2Vec_training_samples(tokenizated_text, context_window=5, print_status=False, status_verbose=0):
	"""
	Función que crea las muestras de entrenamiento para un modelo de embeddings tipo Word2Vec a 
	partir de un texto tokenizado. El sampleo se realiza en base a una ventana contextual parametrizable.

	Embedding basado en skip-gram y sin negative sampling.

	Parameters
	----------

	tokenizated_text : list of str or list of int
		Texto tokenizado en palabras o identificadores númericos enteros.

	context_window : int, optional
		Cantidad de palabras o tokens que conformarán el contexto de una palabra target (Por defecto es 5)

	print_status : bool, optional
		Booleano para determinar si se desea un feedback del status de procesamiento (Por defecto es False)

	status_verbose : int, optional
		Modo de impresión del feedback, verbose = 0 corresponde a un mensaje genérico de Loading estático que imprime un mensaje 
		en la primer y último ciclo, verbose > 0 imprime una barra de progreso en cada ciclo (Por defecto es 0).

	Returns
	-------

	X : list of list of str or list of list of int
		Lista de muestras de entrada de entrenamiento 

	Y : list of list of str or list of list of int
		Lista de muestras de salida de entrenamiento

	"""
	N = len(tokenizated_text)
	X, Y = [], []
	for i in range(N):
		nbr_inds = list(range(max(0, i - context_window), i)) + list(range(i + 1, min(N, i + context_window + 1)))
		for j in nbr_inds:
			X.append(tokenizated_text[i])
			Y.append(tokenizated_text[j])

		if print_status:
			status_log(i, N, 'Generating samples for Word2Vec embedding',increment_percentaje=20,memory_warnings=False)

	return (X, Y)

def generate_Word2Vec_training_samples_with_negative_sampling(tokenizated_text, context_window=5, K_window=20, print_status=False, status_verbose=0):
	"""
	Función que crea las muestras de entrenamiento para un modelo de embeddings tipo Word2Vec a partir de un texto tokenizado.
	El sampleo se realiza en base a una ventana contextual de positivos y un valor de selección de negativos (K) parametrizable.

	Embedding basado en skip-gram y con negative sampling.

	Parameters
	----------

	tokenizated_text : list of str or list of int
		Texto tokenizado en palabras o números según el identificador que tengan dentro de un determinado vocabulario.

	context_window : int, optional
		Cantidad de palabras o tokens a la izquierda y derecha o tokens que conformaran el contexto de una palabra o target 
		(Por defecto es 5).

	K_window : int, optional
		Cantidad de palabras o tokens que conformaran el "no contexto" de una palabra target, elementos
		que se obtienen al azar del texto tokenizado y que no esten dentro de las consideradas en el contexto.
		(Por defecto es 5).

	print_status : bool, optional
		Booleano para determinar si se desea un feedback del status de procesamiento (Por defecto es False)

	status_verbose : int, optional
		Modo de impresión del feedback, verbose = 0 corresponde a un mensaje genérico de Loading estático que imprime un mensaje 
		en la primer y último ciclo, verbose > 0 imprime una barra de progreso en cada ciclo (Por defecto es 0).

	Returns
	-------

	X : list of tuples of list of str or list of tuples of list of int
		Lista de muestras de entrada de entrenamiento en forma de tupla (Input 1 y Input 2).

	Y : list of list of str or list of list of int
		Lista de muestras de salida de entrenamiento.

	"""
	N = len(tokenizated_text)
	X, Y = [], []
	for i in range(N):
		nbr_inds = list(range(max(0, i - context_window), i)) + list(range(i + 1, min(N, i + context_window + 1)))
		if i < N - 1:
			for indexValue in nbr_inds:
				X.append((tokenizated_text[i], tokenizated_text[indexValue]))
				Y.append(1)

			for _ in range(K_window):
				negative_sample_index = np.random.randint(0, N - 1)
				while negative_sample_index in nbr_inds:
					negative_sample_index = np.random.randint(0, N - 1)

				random_word = tokenizated_text[negative_sample_index]
				X.append((tokenizated_text[i], random_word))
				Y.append(0)

		if print_status:
			status_log(i, N, 'Generating samples for Word2Vec with negative sampling embedding training',increment_percentaje=20,memory_warnings=False)

	return (X, Y)

def get_fastText_representation_for_word_ngrams_vectors(word_ngrams_vectors, mode='sum', weighted_result=False, print_status=False, status_verbose=0):
	"""
	Función para obtener los embeddings tipo FastText de una palabra de entrada ya tokenizada, enegramada(existe ese término?)
	y vectorizada, a partir de un modo de "construcción" del embedding de la palabra.

	Modos de formación del embedding:

		sum : El embedding de la palabra se forma al sumar vectorialmente cada dimensión respectiva de los
		embeddings de los ngramas.

		avg : El embedding de la palabra se forma al promediar vectorialmente cada dimensión respectiva de los
		embeddings de los ngramas.

		min : El embedding de la palabra se forma vectorialmente con el valor mínimo de cada dimensión respectiva de los
		embeddings de los ngramas.

		max : El embedding de la palabra se forma vectorialmente con el valor máximo de cada dimensión respectiva de los
		embeddings de los ngramas.

		none : No se realiza ninguna "construcción" de embedding de palabras y solo se devuelven los vectores
		de cada ngrama de cada palabra

	Parameters
	----------
	
	word_ngrams_vectors : list of list of numpy arrays
		Palabra separada en vectores de sus ngramas a la cual se le desa conocer su vector fasText

	mode : str, optional
		Modo de "construcción" del vector final que representa la palabra ingresada (Por defecto es "sum")

	weighted_result : bool, optional
		Booleano que indica si se desea ponderar la influencia de cada ngrama por la cantidad total de
		ngramas, cada vector de cada ngramas aporta solo un % de su valor dependiendo de la cantidad de 
		ngramas totales.

	print_status : bool, optional
		Booleano para determinar si se desea un feedback del status de procesamiento (Por defecto es False)

	status_verbose : int, optional
		Modo de impresión del feedback, verbose = 0 corresponde a un mensaje genérico de Loading estático que imprime un mensaje 
		en la primer y último ciclo, verbose > 0 imprime una barra de progreso en cada ciclo (Por defecto es 0).

	Returns
	-------

	final_word_vector : numpy array or list of list of list of numpy array
		Vector o vectores que representan la palabra analizada.

	"""
	avaible_modes = ['sum', 'avg', 'min', 'max', 'none']
	if mode not in avaible_modes:
		raise NameError("The FastText mode ('{}') that you passed is not valid, options are {}".format(str(mode), str(avaible_modes)))
	if weighted_result:
		ngrams_count = len(word_ngrams_vectors)
		word_ngrams_vectors = np.multiply(1 / ngrams_count, word_ngrams_vectors)
	if mode == 'sum':
		final_word_vector = np.sum(word_ngrams_vectors, axis=0)
	elif mode == 'avg':
		final_word_vector = np.mean(word_ngrams_vectors, axis=0)
	elif mode == 'max':
		final_word_vector = np.max(word_ngrams_vectors, axis=0)
	elif mode == 'min':
		final_word_vector = np.min(word_ngrams_vectors, axis=0)
	elif mode == 'none':
		final_word_vector = np.array(word_ngrams_vectors)
	return final_word_vector

def get_fastText_representation_for_sentence_ngrams_vectors(sentence_ngrams_vectors, mode='sum', weighted_result=False, print_status=False, status_verbose=0):
	"""
	Función para obtener los embeddings tipo FastText de una oración de entrada. cuyas palabras ya han sido tokenizada,
	enegramadas(existe ese término?)y vectorizadas, a partir de un modo de "construcción" del embedding de la palabra.

	Modos de formación del embedding:

		Modos de formación del embedding:

		sum : El embedding de la palabra se forma al sumar vectorialmente cada dimensión respectiva de los
		embeddings de los ngramas.

		avg : El embedding de la palabra se forma al promediar vectorialmente cada dimensión respectiva de los
		embeddings de los ngramas.

		min : El embedding de la palabra se forma vectorialmente con el valor mínimo de cada dimensión respectiva de los
		embeddings de los ngramas.

		max : El embedding de la palabra se forma vectorialmente con el valor máximo de cada dimensión respectiva de los
		embeddings de los ngramas.

		none : No se realiza ninguna "construcción" de embedding de palabras y solo se devuelven los vectores
		de cada ngrama de cada palabra

	Parameters
	----------
	sentence_ngrams_vectors : list of list of list of numpy arrays
		Palabras a las cuales se le desa conocer sus vectores de embedding

	embedding_dictionary : dict
		Diccionario que mapea los ngramas a su respectivo vector de embeddings

	mode : str, optional
		Modo de "construcción" del vector final que representa la palabra ingresada(Por defecto es "sum")

	weighted_result : bool, optional
		Booleano que indica si se desea ponderar la influencia de cada ngrama por la cantidad total de
		ngramas, cada vector de cada ngramas aporta solo un % de su valor dependiendo de la cantidad de 
		ngramas totales.

	print_status : bool, optional
		Booleano para determinar si se desea un feedback del status de procesamiento (Por defecto es False)

	status_verbose : int, optional
		Modo de impresión del feedback, verbose = 0 corresponde a un mensaje genérico de Loading estático que imprime un mensaje 
		en la primer y último ciclo, verbose > 0 imprime una barra de progreso en cada ciclo (Por defecto es 0).

	Returns
	-------
	sentence_embeddings : list of numpy array or list of list of list of list of numpy array
		Vector o vectores que representan las palabras de la oración analizada.

	"""
	sentence_embeddings = []
	total_data = len(sentence_ngrams_vectors)
	for word_index, word in enumerate(sentence_ngrams_vectors):
		sentence_embeddings.append(get_fastText_representation_for_word_ngrams_vectors(word, mode, weighted_result))
		if print_status:
			status_log(word_index, total_data, 'Getting FastText Embedding vector for Sentence',increment_percentaje=20,memory_warnings=False)

	return sentence_embeddings

def get_fastText_representation_for_list_of_sentences_ngrams_vectors(list_of_sentence_ngrams_vectors, mode='sum', weighted_result=False, print_status=False, status_verbose=0):
	"""
	Función para obtener los embeddings tipo FastText de una lista de oraciones de entrada. cuyas palabras ya han sido tokenizada,
	enegramadas(existe ese término?)y vectorizadas, a partir de un modo de "construcción" del embedding de la palabra.

	Modos de formación del embedding:

		Modos de formación del embedding:

		sum : El embedding de la palabra se forma al sumar vectorialmente cada dimensión respectiva de los
		embeddings de los ngramas.

		avg : El embedding de la palabra se forma al promediar vectorialmente cada dimensión respectiva de los
		embeddings de los ngramas.

		min : El embedding de la palabra se forma vectorialmente con el valor mínimo de cada dimensión respectiva de los
		embeddings de los ngramas.

		max : El embedding de la palabra se forma vectorialmente con el valor máximo de cada dimensión respectiva de los
		embeddings de los ngramas.

		none : No se realiza ninguna "construcción" de embedding de palabras y solo se devuelven los vectores
		de cada ngrama de cada palabra

	Parameters
	----------
	list_of_sentence_ngrams_vectors : list of list of list of list of numpy arrays
		Palabras a las cuales se le desa conocer sus vectores de embedding

	embedding_dictionary : dict
		Diccionario que mapea los ngramas a su respectivo vector de embeddings

	mode : str, optional
		Modo de "construcción" del vector final que representa la palabra ingresada(Por defecto es "sum")

	weighted_result : bool, optional
		Booleano que indica si se desea ponderar la influencia de cada ngrama por la cantidad total de
		ngramas, cada vector de cada ngramas aporta solo un % de su valor dependiendo de la cantidad de 
		ngramas totales.

	print_status : bool, optional
		Booleano para determinar si se desea un feedback del status de procesamiento (Por defecto es False)

	status_verbose : int, optional
		Modo de impresión del feedback, verbose = 0 corresponde a un mensaje genérico de Loading estático que imprime un mensaje 
		en la primer y último ciclo, verbose > 0 imprime una barra de progreso en cada ciclo (Por defecto es 0).

	Returns
	-------
	sentence_embeddings : list of numpy array or list of list of list of list of numpy array
		Vector o vectores que representan las palabras de la oración analizada.

	"""
	list_of_sentences_embeddings = []
	total_data = len(list_of_sentence_ngrams_vectors)
	for p_index, sentence in enumerate(list_of_sentence_ngrams_vectors):
		sentence_embedding = get_fastText_representation_for_sentence_ngrams_vectors(sentence, mode, weighted_result)
		list_of_sentences_embeddings.append(sentence_embedding)
		if print_status:
			status_log(p_index, total_data, 'Getting FastText Embedding vector for Sentences List',increment_percentaje=20,memory_warnings=False)

	return list_of_sentences_embeddings

def contextualize_sentence_embeddings_with_surroundings(embedded_sentence, mode='add_average', contextualize_window=2, print_status=False, status_verbose=0):
	"""
	(Experimental)
	Función para re-generar los embeddings de cada palabra de una oración a partir de la influencia de las otras 
	palabras de la misma oración en base a un determinado modo. A este proceso se le llamará "contextualización",
	y generará embeddings diferentes para una misma palabra dependiendo de la influencia de otras.

	Modos de contextualización de embeddings:

		add_average : El embedding final de las palabras se obtiene al sumar el promedio de cada
		dimensión de los vectores de las otras palabras de la oración.

		add_windowed_average : El embedding final de las palabras se obtiene al sumar el promedio de cada
		dimensión de los vectores de las n palabras a la derecha y izquierda de la palabra
		evaluada en la oración.

	Parameters
	----------
		embedded_sentence : list of numpy array
			Lista de vectores que representan los embedding de cada palabra de una oración.

		mode : str
			Modo de "contextualización" del vector final que representa la palabra ingresada.

		contextualize_window : int, optional
			Cantidad de palabras o vectores tanto a la derecha como la izqquierda que serán utilizados 
			para contextualizar el embedding de una palabra. Se utiliza solo cuando se elige el modo de
			contextualización "add_windowed_average" (Por defecto es 2)

		print_status : bool, optional
			Booleano para determinar si se desea un feedback del status de procesamiento (Por defecto es False)

		status_verbose : int, optional
			Modo de impresión del feedback, verbose = 0 corresponde a un mensaje genérico de Loading estático que imprime un mensaje 
			en la primer y último ciclo, verbose > 0 imprime una barra de progreso en cada ciclo (Por defecto es 0).

	Returns
	-------
		new_words_embeddings : list of numpy array
			Nuevos vectores que representan las palabras de la oraciones analizadas.

	"""
	avaible_modes = [
	 'add_average', 'add_windowed_average']
	if mode not in avaible_modes:
		raise NameError("The contextualizing mode ('{}') that you passed is not valid, options are {}".format(str(mode), str(avaible_modes)))
	if mode == 'add_average':
		new_words_embeddings = []
		words_in_sentence = len(embedded_sentence)
		for index, embedded_word in enumerate(embedded_sentence):
			left_context = embedded_sentence[0:index]
			if index != words_in_sentence - 1:
				if index != 0:
					right_context = embedded_sentence[index + 1:]
					context_avg = np.concatenate((left_context, right_context), axis=0)
					context_avg = np.average(context_avg, axis=0)
					word_new_embedding = embedded_word + context_avg
				else:
					if index == 0:
						right_context = embedded_sentence[index + 1:]
						context_avg = np.average(right_context, axis=0)
						word_new_embedding = embedded_word + context_avg
					else:
						context_avg = np.average(left_context, axis=0)
						word_new_embedding = embedded_word + context_avg
					new_words_embeddings.append(word_new_embedding)
				if print_status:
					status_log(index, words_in_sentence, 'Contextualizing words embeddings',increment_percentaje=20,memory_warnings=False)

	if mode == 'add_windowed_average':
		new_words_embeddings = []
		words_in_sentence = len(embedded_sentence)
		for index, embedded_word in enumerate(embedded_sentence):
			left_context = embedded_sentence[max(0, index - contextualize_window):index]
			if index != words_in_sentence - 1:
				if index != 0:
					right_context = embedded_sentence[index + 1:index + 1 + contextualize_window]
					context_avg = np.concatenate((left_context, right_context), axis=0)
					context_avg = np.average(context_avg, axis=0)
					word_new_embedding = embedded_word + context_avg
				else:
					if index == 0:
						right_context = embedded_sentence[index + 1:index + 1 + contextualize_window]
						context_avg = np.average(right_context, axis=0)
						word_new_embedding = embedded_word + context_avg
					else:
						context_avg = np.average(left_context, axis=0)
						word_new_embedding = embedded_word + context_avg
					new_words_embeddings.append(word_new_embedding)
				if print_status:
					status_log(index, words_in_sentence, 'Contextualizing words embeddings',increment_percentaje=20,memory_warnings=False)

	return new_words_embeddings

def contextualize_sentences_list_embeddings_with_surroundings(embedded_sentence_list, mode='add_average', context_window=2, print_status=False, status_verbose=0):
	"""
	(Experimental)
	Función para re-generar los embeddings de cada palabra de una lista de oraciones a partir de la influencia de las otras 
	palabras de la mismas oraciones en base a un determinado modo. A este proceso se le llamará "contextualización",
	y generará embeddings diferentes para una misma palabra dependiendo de la influencia de otras.

	Modos de contextualización de embeddings:

		add_average : El embedding final de las palabras se obtiene al sumar el promedio de cada
		dimensión de los vectores de las otras palabras de la oración.

		add_windowed_average : El embedding final de las palabras se obtiene al sumar el promedio de cada
		dimensión de los vectores de las n palabras a la derecha y izquierda de la palabra
		evaluada en la oración.

	Parameters
	----------

	embedded_sentence_list : list of list of numpy array
		Lista de listas de vectores que representan los embedding de cada palabra de oraciones.

	mode : str
		Modo de "contextualización" del vector final que representa la palabra ingresada.

	contextualize_window : int, optional
		Cantidad de palabras o vectores tanto a la derecha como la izqquierda que serán utilizados 
		para contextualizar el embedding de una palabra. Se utiliza solo cuando se elige el modo de
		contextualización "add_windowed_average" (Por defecto es 2)

	print_status : bool, optional
		Booleano para determinar si se desea un feedback del status de procesamiento (Por defecto es False)

	status_verbose : int, optional
		Modo de impresión del feedback, verbose = 0 corresponde a un mensaje genérico de Loading estático que imprime un mensaje 
		en la primer y último ciclo, verbose > 0 imprime una barra de progreso en cada ciclo (Por defecto es 0).

	Returns
	-------
	new_embedding_sentences : list of list of numpy array
		Lista de nuevos vectores que representan las palabras de la oraciones analizadas.

	"""
	new_embedding_sentences = []
	total_data = len(embedded_sentence_list)
	for p_index, embedded_sentence in enumerate(embedded_sentence_list):
		sentence_new_embedding = contextualize_sentence_embeddings_with_surroundings(embedded_sentence, mode, context_window)
		new_embedding_sentences.append(sentence_new_embedding)
		if print_status:
			status_log(p_index, total_data, 'Contextualizing words embedding',increment_percentaje=20,memory_warnings=False)

	return new_embedding_sentences

def condense_sentence_embedding(embedded_sentence, condensing_mode='sum', weighted_result=False, print_status=False, status_verbose=0):
	"""
	(Experimental)
	Función para representar una oración como un solo vector a partir de la "condensación" o
	"compresión" de los embeddings de cada palabra.

	Modos de condesación de embeddings:

		sum : El embedding de la oración se forma al sumar vectorialmente cada dimensión respectiva de los
		embeddings de las palabras.

		avg : El embedding de la oración se forman al promediar vectorialmente cada dimensión respectiva de los
		embeddings de las palabras.

	Parameters
	----------
		embedded_sentence : list of numpy array
			Lista de vectores que representan los embedding de cada palabra de una oración.

		condensing_mode : str, optional
			Modo de "condensación" del vector final que representa la palabra ingresada (Por defectos es "sum").

		weighted_result : bool, optional
			Booleano que indica si se desea ponderar la influencia de cada embedding por la cantidad total de
			embedding, cada embedding de cada palabra aporta solo un % de su valor dependiendo de la cantidad de 
			palabras totales.

		print_status : bool, optional
			Booleano para determinar si se desea un feedback del status de procesamiento (Por defecto es False)

		status_verbose : int, optional
			Modo de impresión del feedback, verbose = 0 corresponde a un mensaje genérico de Loading estático que imprime un mensaje 
			en la primer y último ciclo, verbose > 0 imprime una barra de progreso en cada ciclo (Por defecto es 0).

	Returns
	-------
		sentence_condensed_embedding : numpy array
			Vector que representa la oración comprimida.

	"""
	condensing_modes = [
	 'sum', 'avg']
	if condensing_mode not in condensing_modes:
		raise NameError("The condensing mode ('{}') that you passed is not valid, options are {}".format(str(condensing_mode), str(condensing_modes)))
	if weighted_result:
		word_count = len(embedded_sentence)
		embedded_sentence = np.multiply(1 / word_count, embedded_sentence)
	if condensing_mode == 'sum':
		sentence_condensed_embedding = np.sum(embedded_sentence, axis=0)
	if condensing_mode == 'avg':
		sentence_condensed_embedding = np.average(embedded_sentence, axis=0)
	return sentence_condensed_embedding

def condense_sentences_list_embedding(embedded_sentence_list, condensation_mode='sum', print_status=False, status_verbose=0):
	"""
	(Experimental)
	Función para representar una lista de oraciones como un solo vector a partir de la "condensación" o
	"compresión" de los embeddings de cada palabra.

	Modos de condesación de embeddings:

		sum : El embedding de la oración se forma al sumar vectorialmente cada dimensión respectiva de los
		embeddings de las palabras.

		weighted_sum : El embedding de la oración se forma al sumar vectorialmente cada dimensión respectiva de los
		embeddings de las palabras de manera ponderada, donde cada vector de cada palabra aporta solo un % de sus
		valores dependiendo de la cantidad de palabras totales.

		avg : El embedding de la oración se forman al promediar vectorialmente cada dimensión respectiva de los
		embeddings de las palabras.

		weighted_avg : El embedding de la oración se forman al promediar vectorialmente cada dimensión respectiva de los
		embeddings de las palabras de manera ponderada, donde cada vector de cada palabra aporta solo un % de sus
		valores dependiendo de la cantidad de palabras totales.

	Parameters
	----------
		embedded_sentence_list : list of list of numpy array
			Lista de listas de vectores que representan los embedding de cada palabra de una oración.

		condensing_mode : str, optional
			Modo de "condensación" del vector final que representa la palabra ingresada (Por defectos es "sum").

		print_status : bool, optional
			Booleano para determinar si se desea un feedback del status de procesamiento (Por defecto es False)

		status_verbose : int, optional
			Modo de impresión del feedback, verbose = 0 corresponde a un mensaje genérico de Loading estático que imprime un mensaje 
			en la primer y último ciclo, verbose > 0 imprime una barra de progreso en cada ciclo (Por defecto es 0).

	Returns
	-------
		new_embedded_sentence_list : list of numpy array
			Lista de vectores que representan las oraciones comprimidas.

	"""
	new_embedded_sentence_list = []
	total_data = len(embedded_sentence_list)
	for p_index, sentence in enumerate(embedded_sentence_list):
		new_embedded_sentence_list.append(condense_sentence_embedding(sentence, condensation_mode))
		if print_status:
			status_log(p_index, total_data, 'Condensing embedding vectors for sentences',increment_percentaje=20,memory_warnings=False)

	return new_embedded_sentence_list

def cosine_similarity(u, v):
	"""
	Función que calcula la similitud coseno entre 2 vectores (Análisis espacial que relaciona la distancia,
	longitud y angulo entre 2 vectores).

	Parameters
	----------
		v : numpy array
			Primer vector del análisis con dimensión (n, )

		u : numpy array
			Segundo vector del análisis con dimensión (n, )

	Returns
	-------
		cosine_similarity : float
			Valor de la similitud coseno, donde 0 denosta ninguna similutd y 1 una completa similitud.
	"""
	dot_prod = np.dot(u, v)
	norm_u = np.sqrt(np.sum(np.power(u, 2)))
	norm_v = np.sqrt(np.sum(np.power(v, 2)))
	cosine_similarity = np.divide(dot_prod, norm_u * norm_v)
	return float(cosine_similarity)

def find_similar_words(input_word, word_to_vec_map, closest_words_num=3, print_status=False, status_verbose=0):
	"""
	Función que busca las "closest_words_num" palabras más similares a una de entrada. La salida
	esta ordenada de palabra mas similar a palabra más diferente.

	NOTA: El parámetro "word_to_vec_map" que hace referencia a un diccionario que mapea las palabras
	a sus correspondientes emmbedding, en el caso de FastText, debe ser construido en base a un texto
	de referencia o un diccionario pre-creado, ya que el embedding medular se realiza en base a los vectores
	de los ngramas, diccionario que para este proposito no sirve.

	Parameters
	----------
		input_word : str
			Palabra a analizar 

		word_to_vec_map : dict
			Diccionario de datos que mapea una palabra a su vector de embeddings

		closest_words_num : int
			Cantidad de palabras similares que se desea obtener.

	Returns
	-------
		closest_words_pairs : List of [str ,float]
			Lista de pares que contienen las "closest_words_dict" palabras más similares encontradas
			junto a sus similitudes coseno.
	"""
	closest_words = []
	closest_cosines_sims = []
	input_word_vec = word_to_vec_map[input_word]
	for w in word_to_vec_map.keys():
		if w == input_word:
			pass
		else:
			cosine_sim = cosine_similarity(word_to_vec_map[w], input_word_vec)
			if len(closest_words) < closest_words_num:
				closest_words.append(w)
				closest_cosines_sims.append(cosine_sim)
			else:
				min_sim = min(closest_cosines_sims)
				if cosine_sim > min_sim:
					indx = closest_cosines_sims.index(min_sim)
					closest_cosines_sims[indx] = cosine_sim
					closest_words[indx] = w

	closest_words_pairs = []
	for indx, _ in enumerate(closest_words):
		closest_words_pairs.append((closest_words[indx], closest_cosines_sims[indx]))

	closest_words_pairs.sort(key=(lambda x: x[1]), reverse=True)
	return closest_words_pairs

def complete_analogy(word_a, word_b, word_c, word_to_vec_map):
	"""
	Función para evaluar la correctitud y calidad de los embedding al realizar una analogía
	entre tres palabras de la siguiente manera:

	A es a B como C es a ____

	Ej. Casa es a arquitectura como Árbol es a *naturaleza*, donde naturaleza es la palabra esperada
	que debería arrojar el análisis a partir de un diccionario que contiene el vocabulario de una tarea de NLP.

	NOTA: El parámetro "word_to_vec_map" que hace referencia a un diccionario que mapea las palabras
	a sus correspondientes emmbedding, en el caso de FastText, debe ser construido en base a un texto
	de referencia o un diccionario pre-creado, ya que el embedding medular se realiza en base a los vectores
	de los ngramas, diccionario que para este proposito no sirve.

	Parameters
	----------
		word_a : str
			Primera palabra del análisis, representa el "A" de la explicación de esta función.

		word_b :str
			Segunda palabra del análisis, representa el "B" de la explicación de esta función.

		word_c :str
			Tercera palabra del análisis, representa el "C" de la explicación de esta función.

		word_to_vec_map : dir
			Diccionario que mapea palabras a sus correspondientes embedding

	Returns
	-------
		best_word : str
			Palabra que más se ajusta y responde a la analogía propuesta

	"""
	e_a = word_to_vec_map[word_a]
	e_b = word_to_vec_map[word_b]
	e_c = word_to_vec_map[word_c]
	words = word_to_vec_map.keys()
	max_cosine_sim = -100
	best_word = None
	for w in words:
		if w in [word_a, word_b, word_c]:
			pass
		else:
			cosine_sim = cosine_similarity(e_b - e_a, word_to_vec_map[w] - e_c)
			if cosine_sim > max_cosine_sim:
				max_cosine_sim = cosine_sim
				best_word = w

	return best_word

def evaluate_embedding_dictionary_quality(word_to_vec_map, closest_words_num=3, print_status=True, status_verbose=0):
	"""
	Función que busca las "closest_words_num" palabras más similares para cada elemento dentro de un
	diccionario.- La salida está ordenada de palabra más similar a palabra más diferente.

	NOTA: El parámetro "word_to_vec_map" que hace referencia a un diccionario que mapea las palabras
	a sus correspondientes emmbedding, en el caso de FastText, debe ser construido en base a un texto
	de referencia o un diccionario pre-creado, ya que el embedding medular se realiza en base a los vectores
	de los ngramas, diccionario que para este proposito no sirve.

	----------
		word_to_vec_map : dir
			Diccionario que mapea palabras a sus correspondientes embedding

		word_to_vec_map :str
			Diccionario de embeddings de ngramas.

		print_status : bool, optional
			Booleano para determinar si se desea un feedback del status de procesamiento (Por defecto es False)

		status_verbose : int, optional
			Modo de impresión del feedback, verbose = 0 corresponde a un mensaje genérico de Loading estático que imprime un mensaje 
			en la primer y último ciclo, verbose > 0 imprime una barra de progreso en cada ciclo (Por defecto es 0).

	Returns
	-------
		closest_words : list of [str,[str,float]]
			Lista con pares palabras de un diccionario, sus palabras más similares y el valor de
			la similitud coseno entre ellas.

	"""
	closest_words = []
	total_data = len(word_to_vec_map.keys())
	for p_index, word in enumerate(word_to_vec_map.keys()):
		similar_words = find_similar_words(word, word_to_vec_map, closest_words_num)
		closest_words.append((word, similar_words))
		if print_status:
			status_log(p_index, total_data, 'Getting {} Closest Embedding Vectors for Text Words'.format(str(closest_words_num)),increment_percentaje=20,memory_warnings=False)

	return closest_words

#TODO: Documentar. Se deja pendiente debido a que ya estaba hecha y se perdio al reemplazar el archivo equivocado (Ragequit).
def generate_embedding_dictionary(embedding_matrix, element2index_map, embedding_dims, embedding_name, save_to_file=True, saving_loc='./'):
	embedding_dict = {w:embedding_matrix[idx] for w, idx in element2index_map.items()}
	if save_to_file:
		save_pickle(embedding_dict, embedding_name + '_dictionary', saving_loc)
	return embedding_dict

def format_data_for_elmo_embedding(sentenceList, elmo_mode='tokens', print_status=False, status_verbose=0):
	"""
	Función que formatea y transforma una lista de oraciones tokenizadas y sus palabras a una representación de numpyarray
	acorde al input de una capa de embedding elmo de TFHub a partir de alguno de sus modos: "elmo_tokens" o "elmo_text".

	Parameters
	----------
		SentenceList : list of list of str
			Lista de oraciones a las cuales se desea formatear para ser validas con la capa de embedding elmo de TFHub.

		elmo_mode : str, optional
			Modo de la capa de embeddings elmo. (Por defecto es "tokens")

		print_status : bool, optional
			Booleano para determinar si se desea un feedback del status de procesamiento (Por defecto es False)

		status_verbose : int, optional
			Modo de impresión del feedback, verbose = 0 corresponde a un mensaje genérico de Loading estático que imprime un mensaje 
			en la primer y último ciclo, verbose > 0 imprime una barra de progreso en cada ciclo (Por defecto es 0).

	Returns
	-------
		X_Embedding : list list of numpy array
			Lista de oraciones con sus palabras representadas en vectores de embedding.

	"""
	if elmo_mode == 'text':
		texts = []
		total_data = len(sentenceList)
		for p_index, tokenizated_sentence in enumerate(sentenceList):
			input_text = ''
			sample_sentence_lenght = len(tokenizated_sentence)
			for word_index, word in enumerate(tokenizated_sentence):
				if word_index < sample_sentence_lenght - 1:
					input_text += ' ' + str(word)
					input_text += str(word)

			texts.append(input_text)
			if print_status:
				status_log(p_index, total_data, 'Getting elmo embedding input data',increment_percentaje=20,memory_warnings=False)

		texts = np.array(texts)
		texts = np.reshape(texts, (texts.shape[0], 1))
		return texts
	else:
		if elmo_mode == 'tokens':
			return np.array(sentenceList)
		return 0

class WordVectors:
	r"""'\n    Esta clase permite cargar y utilizar un modelo de word embeddings basado en fastText desde un archivo .pickle\n    contenedor de un diccionario que realiza el mapping entre tokens (palabra o ngrama) y sus respectivos embeddings.\n    '"""

	def __init__(self, ruta_modelo, ngram_min=2, ngram_max=2, unknown_embedding_element='<UNKNOWN_NGRAM>', verbose=False):

		if verbose:
			log.info('Instanciando objeto Embedding: {}'.format(str(ruta_modelo)),extra={"context" : self.__class__.__name__})
		
		self.pre_computed_tokens = {}
		self.diccionario_embedding = load_pickle(ruta_modelo)
		self.ngram_min = ngram_min
		self.ngram_max = ngram_max
		self.unknown_element = unknown_embedding_element
		test_embedding = self._parse_sentences_list_to_fastText([['-']], (self.diccionario_embedding), min_ngram_window=(self.ngram_min),
			max_ngram_window=(self.ngram_max),
			unknown_element=(self.unknown_element))
		self.dim_embedding = len(test_embedding[0][0])
	
	#TODO: Documentar. Se deja pendiente debido a que ya estaba hecha y se perdio al reemplazar el archivo equivocado (Ragequit).
	def _parse_word_to_fastText(self, word, embedding_dictionary, min_ngram_window=2, max_ngram_window=2, unknown_element='<UNKNOWN_NGRAM>', include_wordgram=True, fasttext_mode='sum', weighted_fasttext_mode=False, print_status=False, status_verbose=0):
		token_vector = self.pre_computed_tokens.get(word)
		if(token_vector is None):
			token_ngrams = get_ngrams_for_word(word, min_ngram_window, max_ngram_window, include_wordgram, print_status, status_verbose)
			token_ngrams_vectors = encode_element(token_ngrams, embedding_dictionary, unknown_element)
			token_vector = get_fastText_representation_for_word_ngrams_vectors(token_ngrams_vectors, fasttext_mode, weighted_fasttext_mode, print_status, status_verbose)
			self.pre_computed_tokens.update({word:token_vector})
		return token_vector

	#TODO: Documentar. Se deja pendiente debido a que ya estaba hecha y se perdio al reemplazar el archivo equivocado (Ragequit).
	def _parse_sentence_to_fastText(self, sentence, embedding_dictionary, min_ngram_window=2, max_ngram_window=2, unknown_element='<UNKNOWN_NGRAM>', include_wordgram=True, fasttext_mode='sum', weighted_fasttext_mode=False, print_status=False, status_verbose=0):
		tokens_vectors_list = []
		total_data = len(sentence)
		for p_index, token in enumerate(sentence):
			token_vector = self._parse_word_to_fastText(token, embedding_dictionary, min_ngram_window, max_ngram_window, unknown_element, include_wordgram, fasttext_mode, weighted_fasttext_mode)
			tokens_vectors_list.append(np.array(token_vector))
			if print_status:
				status_log(p_index, total_data, 'Parsing sentences to fastText',increment_percentaje=20,memory_warnings=False)
		return tokens_vectors_list

	#TODO: Documentar. Se deja pendiente debido a que ya estaba hecha y se perdio al reemplazar el archivo equivocado (Ragequit).
	def _parse_sentences_list_to_fastText(self, sentences_list, embedding_dictionary, min_ngram_window=2, max_ngram_window=2, unknown_element='<UNKNOWN_NGRAM>', include_wordgram=True, fasttext_mode='sum', weighted_fasttext_mode=False, print_status=False, status_verbose=0):
		lists_of_tokens_vectors = []
		total_data = len(sentences_list)
		for p_index, sentence in enumerate(sentences_list):
			list_tokens_vectors = self._parse_sentence_to_fastText(sentence, embedding_dictionary, min_ngram_window, max_ngram_window, unknown_element, include_wordgram, fasttext_mode, weighted_fasttext_mode)
			lists_of_tokens_vectors.append(np.array(list_tokens_vectors))
			if print_status:
				status_log(p_index, total_data, 'Parsing sentences list to fastText',increment_percentaje=20,memory_warnings=False)

		return lists_of_tokens_vectors

	def word_predict(self, input, fasttext_mode='sum', weighted_fasttext_mode=False):
		embedding_secuencia = self._parse_word_to_fastText(input, (self.diccionario_embedding), min_ngram_window=(self.ngram_min),
			max_ngram_window=(self.ngram_max),
			unknown_element=(self.unknown_element),
			fasttext_mode=fasttext_mode,
			weighted_fasttext_mode=weighted_fasttext_mode)
		embedding_secuencia = np.array(embedding_secuencia)
		return embedding_secuencia

	def sentence_predict(self, input, fasttext_mode='sum', weighted_fasttext_mode=False):
		embedding_secuencia = self._parse_sentence_to_fastText(input, (self.diccionario_embedding), min_ngram_window=(self.ngram_min),
			max_ngram_window=(self.ngram_max),
			unknown_element=(self.unknown_element),
			fasttext_mode=fasttext_mode,
			weighted_fasttext_mode=weighted_fasttext_mode)
		embedding_secuencia = np.array(embedding_secuencia)
		return embedding_secuencia
				
	def sentence_list_predict(self, input, fasttext_mode='sum', weighted_fasttext_mode=False):
		embedding_secuencia = self._parse_sentences_list_to_fastText(input, (self.diccionario_embedding), min_ngram_window=(self.ngram_min),
			max_ngram_window=(self.ngram_max),
			unknown_element=(self.unknown_element),
			fasttext_mode=fasttext_mode,
			weighted_fasttext_mode=weighted_fasttext_mode)
		embedding_secuencia = np.array(embedding_secuencia)
		return embedding_secuencia
