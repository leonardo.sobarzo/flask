import numpy as np
import math

from Texto.debugging_insights import get_process_status as status_log
from Texto.common import get_ngrams_for_sentence

#Author : Javier D.  

def create_vocabulary(tokens, vocabulary=[], as_ngrams=False, min_ngram_window=2, max_ngram_window=2, 
                    include_wordgram = True, print_status=False, status_verbose=0):
    
    """
    Función que obtiene una lista de palabras o vocabulario encontrado en un texto dado. El vocabulario se construye
    en base a uno previo (que puede estar vacío), agregando palabras o elementos a este si es que estos elementos son nuevos y
    no estaban contenidos ya en el vocabulario.

    Parameters
    ----------
    tokens : list of str
        Texto como conjunto de tokens del cual se desea generar un vocabulario.

    vocabulary : list of str
        Vocabulario base

    as_ngrams : bool, optional
        Booleano que indica si las palabras del texto deben ser tratadas como un conjunto de ngramas, siendo los
        ngramas los elementos que compondran el vocabulario en vez de las palabras(se utiliza para tratamiento
        de datos de embedding tipo fastText)(Por defecto es False).

    min_ngram_window : int, optional
        Dimensión mínima de las ventanas de ngramas a obtener, esto es, la cantidad de caracteres que tendrá 
        un ngrama (Por defecto es 2)

    max_ngram_window : int, optional
        Dimensión máxima de las ventanas de ngramas a obtener, esto es, la cantidad de caracteres que tendrá 
        un ngrama (Por defecto es 2)

    include_wordgram : bool, optional
        Booleano que indica si se quiere agregar el ngrama palabra dentro de los ngramas obtenidos (Por defecto es True). 
        Ej: si se considera solo ngrama de dos caracteres y tenemos la palabra "prueba", si incluimos el ngrama palabra, 
        los ngramas de la palabra dada serían ["<p,"pa","al","la","ab","br","ra","a>","<palabra>"], en caso contrario, 
        si no se incluyera, sería solo ["<p,"pa","al","la","ab","br","ra","a>"]. El comportamiento estandar de fastText 
        considera esto como verdadero.

    print_status : bool, optional
        Booleano para determinar si se desea un feedback del status de procesamiento de la lista dada (por defecto True)

    status_verbose : int, optional representadas por ngramas de la oraciones entregadas
        Modo de impresión del feedback, verbose = 0 corresponde a un mensaje genérico de Loading estático que imprime un mensaje 
        en la primer y último ciclo, verbose > 0 imprime una barra de progreso en cada ciclo (Por defecto es 0).

    Returns
    -------
        vocabulary : list of str
            Lista de palabras o vocabulario nuevo/actualizado.

        
    """

    total_data = len(tokens)
    if (as_ngrams):
        tokens_ngrams = get_ngrams_for_sentence(tokens, min_ngram_window, max_ngram_window, include_wordgram, print_status, status_verbose)
        for token_index, token_ngrams in enumerate(tokens_ngrams):
            for ngram in token_ngrams:
                if (ngram not in vocabulary):
                    vocabulary.append(ngram)
            if (print_status):
                status_log(token_index, total_data, "Collecting new ngrams",increment_percentaje=20,memory_warnings=False)
        return vocabulary
        
    total_data = len(tokens)
    for token_index, token in enumerate(tokens):
        if (token not in vocabulary):
            vocabulary.append(token)
        if (print_status):
                status_log(token_index, total_data, "Looking for new words/ngrams and (re)creating a vocabulary",increment_percentaje=20,memory_warnings=False)
    
    return vocabulary

def create_dictionary_from_vocabulary(vocabulary=[], print_status=False, status_verbose=0):
    """
    Función que obtiene una diccionario de python para mapear las palabras de un vocabulario a través de su indice
    y viceversa.

    Parameters
    ----------
    vocabulary : list of str
        Vocabulario base sobre el cual se trabajará y creara el diccionario.

    print_status : bool, optional
        Booleano para determinar si se desea un feedback del status de procesamiento de la lista dada (por defecto True)

    status_verbose : int, optional representadas por ngramas de la oraciones entregadas
        Modo de impresión del feedback, verbose = 0 corresponde a un mensaje genérico de Loading estático que imprime un mensaje 
        en la primer y último ciclo, verbose > 0 imprime una barra de progreso en cada ciclo (Por defecto es 0).

    Returns
    -------
    indx_to_element_mapping : dict
        Diccionario/mapeo de indices a elementos de un vocabulario.

    element_to_index_mapping : dict
        Diccionario/mapeo de elementos a indices de un vocabulario.
        
    """
    indx_to_element_mapping = {}
    element_to_index_mapping = {}
    total_data = len(vocabulary)
    for i,word in enumerate(vocabulary):
        indx_to_element_mapping[i]= word
        element_to_index_mapping[word] = i
        if (print_status):
            status_log(i, total_data, "Creating vocabulary mappings",increment_percentaje=20,memory_warnings=False)

    return indx_to_element_mapping, element_to_index_mapping

def get_features_from_tokens_list(tokens_list, vocabulary=[], unknown_element="[UNK]", as_ngrams = False,
                            min_ngram_window=2, max_ngram_window=2, include_wordgram = True, 
                            print_status=False, status_verbose=0):
    """
    Función que recibe un texto base tokenizado, crea y devuelve un vocabulario de palabras, el largo de este, 
    un diccionario como mapeo de los indices del vocabulario a la palabra y otro con las palabras a sus respectivos
    indices.

    El vocabulario se construye en base a uno previo (que puede estar vacío), agregando palabras o elementos a este
    si es que estos elementos fueran nuevos y no estubieran contenidos ya en el vocabulario.
    
    Parameters
    ----------
    tokens : str
        Texto como un conjunto de listas de palabras del cual se desea generar un vocabulario.

    vocabulary : list of str, optional
        Vocabulario base sobre el cual se trabajará y agregaran las nuevas palabras o lementos encontrados, 
        puede ser una lista de palabras previamente poblada o vacía. (Por defecto es una lista vacía)

    unknown_element : str, optional
        Elemento que representará a todo elemento que no este contenido dentro del vocabulario. (Por defectos es "[UNK]")

    as_ngrams : bool, optional
        Booleano que indica si las palabras del texto deben ser tratadas como un conjunto de ngramas, siendo los
        ngramas los elementos que compondran el vocabulario en vez de las palabras(se utiliza para tratamiento
        de datos de embedding tipo fastText)(Por defecto es False).

    min_ngram_window : int, optional
        Dimensión mínima de las ventanas de ngramas a obtener, esto es, la cantidad de caracteres que tendra un ngrama (Por defecto es 2)

    max_ngram_window : int, optional
        Dimensión máxima de las ventanas de ngramas a obtener, esto es, la cantidad de caracteres que tendra un ngrama (Por defecto es 2)

    include_wordgram : bool, optional
        Booleano que indica si se quiere agregar el ngrama palabra dentro de los ngramas obtenidos (Por defecto es True). 
        Ej: si se considera solo ngrama de dos caracteres y tenemos la palabra "prueba", si incluimos el ngrama palabra, 
        los ngramas de la palabra dada serían ["<p,"pa","al","la","ab","br","ra","a>","<palabra>"], en caso contrario, 
        si no se incluyera, sería solo ["<p,"pa","al","la","ab","br","ra","a>"]. El comportamiento estandar de fastText 
        considera esto como verdadero.

    print_status : bool, optional
        Booleano para determinar si se desea un feedback del status de procesamiento de la lista dada (por defecto True)

    status_verbose : int, optional recreate_vocabularyesentadas por ngramas de la oraciones entregadas
        Modo de impresión del feedbaccreate_vocabulary verbose = 0 corresponde a un mensaje genérico de Loading estático que imprime un mensaje 
        en la primer y último ciclo, create_vocabularyrbose > 0 imprime una barra de progreso en cada ciclo (Por defecto es 0).

    Returns
    -------
    vocabulary : list of str
        Vocabulario de palabras generado a partir de las palabras de ltexto entregado.

    vocabulary_size : int
        Cantidad de palabras en el vocabulario obtenido.

    indx_to_element_mapping : dict
        Diccionario/mapeo de indices a elementos de un vocabulario.

    element_to_index_mapping : dict
        Diccionario/mapeo de elementos a indices de un vocabulario.

    """
    
    #Inicializadores

    #Elemento 0
    if ("" not in vocabulary):
        vocabulary.append("")
    
    #Elemento 1
    if (unknown_element not in vocabulary):
        vocabulary.append(unknown_element)

    if (as_ngrams):
        vocabulary = create_vocabulary(tokens_list, vocabulary, True, min_ngram_window,
                                        max_ngram_window, include_wordgram,
                                        print_status, status_verbose)
    else:
        vocabulary = create_vocabulary(tokens_list, vocabulary, print_status = print_status, status_verbose = status_verbose)    

    indx_to_element_mapping, element_to_index_mapping = create_dictionary_from_vocabulary(vocabulary, print_status, status_verbose)

    vocabulary_size = len(vocabulary)

    return vocabulary, vocabulary_size, indx_to_element_mapping, element_to_index_mapping

def encode_element(element, dictionary, unknown_element = "[UNK]", print_status = False, status_verbose = 0):
    """
    Función que codifica una palabra u objeto en base a sus elementos atomicos, que pueden ser ngramas (lista de ngramas como
    representación de una palabra), caracteres (string como representaciñón de una palabra) o cualquier otro
    tipo de lista que represente una palabra, incluido así misma como lista si el codificado se basa en palabras.
    La codificación se realiza a partir de un determinado diccionario y una dimensión base.

    Si un elemento no llegase a formar parte del diccionario, se utilizará el elemento auxiliar desconocido
    (unknown_element) para representarlo.

    Parameters
    ----------
    elements : list of object or list of str or str
        Representación en forma de lista (los strings tambien son validos) de una palabra o elemento,
        cuyos elementos atomicos serán codificados con el diccionario.

    dictionary : dict
        Diccionario que se utilizara para codificar los elementos atomicos (De elemento atomico a represetación númerica)

    unknown_element : object or string, optional
        Elemento auxiliar para clasificar y reemplazar a los elementos que son 
        desconocidos o ajenos al diccionario (Por defecto es "[UNK]")

    print_status : bool, optional
        Booleano para determinar si se desea un feedback del status de procesamiento de la lista dada (por defecto True)

    status_verbose : int, optional representadas por ngramas de la oraciones entregadas
        Modo de impresión del feedback, verbose = 0 corresponde a un mensaje genérico de Loading estático que imprime un mensaje 
        en la primer y último ciclo, verbose > 0 imprime una barra de progreso en cada ciclo (Por defecto es 0).

    Returns
    -------

        element_vectors : list of numpy array
            Representación de los elementos que conforman la palabra u objeto en forma vectorial

    """
    elements_vectors = []
    total_data = len(element)
    for index, element in enumerate(element):
        if (element in dictionary):
            elements_vectors.append(dictionary[element])
        else:
            elements_vectors.append(dictionary[unknown_element]) 

        if(print_status):
            status_log(index, total_data, "Encoding element",increment_percentaje=20,memory_warnings=False)

    return elements_vectors
        
def encode_list_of_elements(elements_list, dictionary, unknown_element = "[UNK]", print_status = False, status_verbose = 0):
    
    """
    Función que codifica una lista de palabras o objetos en base a sus elementos atomicos, que pueden ser 
    ngramas (lista de ngramas como representación de una palabra), caracteres (string como representaciñón de una palabra) 
    o cualquier otro tipo de lista que represente una palabra, incluido así misma como lista si el codificado se basa 
    en palabras. La codificación se realiza a partir de un determinado diccionario y una dimensión base.

    Si un elemento no llegase a formar parte del diccionario, se utilizará el elemento auxiliar desconocido
    (unknown_element) para representarlo.

    Parameters
    ----------
        elements_list : list of list of object, list of list of str or list of str
            Lista de representaciones en forma de lista (los strings tambien son validos) de una palabra,
            cuyos elementos que la forman (elementos atomicos) serán codificados por el diccionario.

        dictionary : dict
            Diccionario que se utilizara para codificar las palabras (De elemento atomico a represetación númerica)

        unknown_element : object or string, optional
            Elemento auxiliar para clasificar y reemplazar a los elementos que son 
            desconocidos o ajenos al diccionario (Por defecto es "[UNK]")

        print_status : bool, optional
            Booleano para determinar si se desea un feedback del status de procesamiento de la lista dada (por defecto True)

        status_verbose : int, optional representadas por ngramas de la oraciones entregadas
            Modo de impresión del feedback, verbose = 0 corresponde a un mensaje genérico de Loading estático que imprime un mensaje 
            en la primer y último ciclo, verbose > 0 imprime una barra de progreso en cada ciclo (Por defecto es 0).

    Returns
    -------

        elements_list_vectors : list of numpy array
            Representación de los elementos que conforman la lista de palabras o objetos en forma vectorial

    """

    elements_list_vectors = []
    total_data = len(elements_list)
    for p_index, element in enumerate(elements_list):
        elements_list_vectors.append(encode_element(element, dictionary, unknown_element, print_status=False))
        if(print_status):
            status_log(p_index, total_data, "Encoding elements",increment_percentaje=20,memory_warnings=False)
    return elements_list_vectors

def encode_lists_of_elements(elements_lists, dictionary, unknown_element = "[UNK]", print_status = False, status_verbose = 0):
    
    """
    Función que codifica una lista de listas de palabras o objetos en base a sus elementos atomicos, que pueden ser 
    ngramas (lista de ngramas como representación de una palabra), caracteres (string como representaciñón de una palabra) 
    o cualquier otro tipo de lista que represente una palabra, incluido así misma como lista si el codificado se basa 
    en palabras. La codificación se realiza a partir de un determinado diccionario y una dimensión base.

    Si un elemento no llegase a formar parte del diccionario, se utilizará el elemento auxiliar desconocido
    (unknown_element) para representarlo.


    Parameters
    ----------
        elements_lists : list of list of list of object, list of list of list of str or list of list of str
            Lista de listas de representaciones en forma de lista (los strings tambien son validos) de una palabra,
            cuyos elementos que la forman (elementos atomicos) serán codificados por el diccionario.

        dictionary : dict
            Diccionario que se utilizara para codificar las palabras (De elemento atomico a represetación númerica)

        unknown_element : object or string, optional
            Elemento auxiliar para clasificar y reemplazar a los elementos que son 
            desconocidos o ajenos al diccionario (Por defecto es "[UNK]")

        print_status : bool, optional
            Booleano para determinar si se desea un feedback del status de procesamiento de la lista dada (por defecto True)

        status_verbose : int, optional representadas por ngramas de la oraciones entregadas
            Modo de impresión del feedback, verbose = 0 corresponde a un mensaje genérico de Loading estático que imprime un mensaje 
            en la primer y último ciclo, verbose > 0 imprime una barra de progreso en cada ciclo (Por defecto es 0).
    -------

        elements_lists_vectors : list of numpy array
            Representación de los elementos que conforman las listas de palabras o objetos en forma vectorial

    """
    
    elements_lists_vectors = []
    total_data = len(elements_lists)
    for p_index, elements_list in enumerate(elements_lists):
        elements_lists_vectors.append(encode_list_of_elements(elements_list, dictionary, unknown_element, print_status = False))
        if (print_status):
            status_log(p_index, total_data, "Encoding lists of elements",increment_percentaje=20,memory_warnings=False)
    return elements_lists_vectors 

def calculate_keep_probability_of_word(word, tokenized_text, sample_rate=0.001):

    """
    Función que calcula la probabilidad de que una palabra de un texto tokenizado siga perteneciendo al mismo. 

    Parameters
    ----------
        word : str
            Palabra a la cual se le quiere calcular la probabilidad de mantenerse en existencia.

        tokenized_text : list of str
            Texto tokenizado al cual pertenece la palabra analizada

        sample_rate : float, optional
            Valor float que determina el coeficiente de cuanto afecta a la probabilidad de eliminar una palabra
            en base a su número de ocurrencias. Mientras más bajo, menor probabilidad de que una palabra sea
            conservada(Usando formula de Word2Vec). (Por defecto es 0.001)

    Returns
    -------

        (word, keep_probability) : tuple
            Tupla que representa la palabra y la probabilidad de conservarse dentro del texto tokenizado.

    """
    #sample_rate=0.00001
    word_count = tokenized_text.count(word)
    total_words_count = len(tokenized_text)
    ocurrence_in_corpus = ocurrence_in_corpus = word_count / total_words_count
    ocurrence_in_corpus = ocurrence_in_corpus + 0.00000001
    #keep_probability = (math.sqrt(sample_rate/ocurrence_in_corpus)) #Formula con origen olvidado
    #keep_probability = (math.sqrt(ocurrence_in_corpus/sample_rate)+1)*(sample_rate/ocurrence_in_corpus) #Formula con origen olvidado
    #keep_probability = 1 - math.sqrt(sample_rate/ocurrence_in_corpus) #Formula propuesta por Mikolov en paper de NS -> Calcula la probabilidad de subsamplear una palabra
    keep_probability = abs((ocurrence_in_corpus - sample_rate)/ocurrence_in_corpus) - math.sqrt(sample_rate/ocurrence_in_corpus) #Formula propuesta en paper de Word2Vec -> Calcula la probabilidad de subsamplear una palabra
    if(keep_probability < 0):
        keep_probability = 0
    if(keep_probability > 1):
        keep_probability = 1
    return (word,  1 - keep_probability)

def calculate_keep_probability_of_words_in_sentence(tokenized_sentence, sample_rate = 0.001,
                                                        print_status = False, status_verbose = 0):


    """
    Función que calcula la probabilidad de que las palabras de un texto tokenizado siga perteneciendo 
    al mismo (de lo contrario se elimina del texto), técnica llamada sub-sampling. 

    Parameters
    ----------
        tokenized_text : list of str
            Texto tokenizado al cual se le quiere realizar sub-sampling

        sample_rate : float, optional
            Valor float que determina el coeficiente de cuanto afecta a la probabilidad de eliminar una palabra
            en base a su número de ocurrencias. Mientras más bajo, menor probabilidad de que una palabra sea
            conservada(Usando formula de Word2Vec). (Por defecto es 0.001)

        print_status : bool, optional
            Booleano para determinar si se desea un feedback del status de procesamiento de la lista dada (por defecto True)

        status_verbose : int, optional representadas por ngramas de la oraciones entregadas
            Modo de impresión del feedback, verbose = 0 corresponde a un mensaje genérico de Loading estático que imprime un mensaje 
            en la primer y último ciclo, verbose > 0 imprime una barra de progreso en cada ciclo (Por defecto es 0).

    Returns
    -------

        tuples_list : tuple
            Texto tokenizado en tuplas, donde cada tupla representa una palabra con su respectiva probabilidad de ser conservada
            en el texto.

    """

    tuples_list = []
    saw_words = []
    words_list_count = len(tokenized_sentence)

    for wordIndex,word in enumerate(tokenized_sentence):
        if (word not in saw_words):
            tuple_of_word = calculate_keep_probability_of_word(word, tokenized_sentence, sample_rate)
            saw_words.append(word)
            tuples_list.append(tuple_of_word)
        if(print_status):
            status_log(wordIndex, words_list_count, "Calculating ocurrence frequency of words in text",increment_percentaje=20,memory_warnings=False)

    return tuples_list

def apply_probability_filter_to_word(word_tuple):

    """
    Función que aplica un filtro de conservación a una determinada palabra a partir de la probabilidad de conservación
    que se le calculó. El filtro consiste en generar un valor entre 0 y 1, que en el caso de ser mayor o igual a la probabilidad
    de la palabra, la palabra es conservada en el texto, caso contrario, es eliminada.

    La respuesta de esta función es un booleano que indica si una palabra se conserva o no.  

    Parameters
    ----------
        word_tuple : tuple
            Tupla que representa la palabra y la probabilidad de ser conservada.

    Returns
    -------

        is_kept : bool
            Booleano que indica si una palabra se conserva o es eliminada.

    """

    #print ("word:", word_tuple[0], " tuple prob: ",word_tuple[1],"\n")
    keep_probability = word_tuple[1]
    if keep_probability > 1:
        keep_probability = 1
    is_kept = (np.random.uniform(0, 1.0) < keep_probability)
    #is_Kept = 0.001 <= keep_probability
    return is_kept

def filter_by_words_ocurrence_probability(words_frequency_tuples_list,
                                        print_status = False, status_verbose = 0):

    """
    Función que aplica un filtro de conservación a un texto tokenizado en tuplas de palabras a partir de la probabilidad de conservación
    que se le calculó. El filtro consiste en generar un valor entre 0 y 1, que en el caso de ser mayor o igual a la probabilidad
    de la palabra, la palabra es conservada en el texto, caso contrario, es eliminada.

    Parameters
    ----------
        words_frequency_tuples_list : tuple
            Texto tokenizado en tuplas de palabras con sus probabilidades de conservación

    Returns
    -------

        new_tokenized_text : list of str
            Nuevo texto tokenizado con las palabras ya filtradas segun su probabilidad de conservación.

    """

    new_tokenized_text = []

    words_frequency_tuples_count = len(words_frequency_tuples_list)

    for wordTupleIndex,wordTuple in enumerate(words_frequency_tuples_list):
        if (apply_probability_filter_to_word(wordTuple)):
            new_tokenized_text.append(wordTuple[0])
            
        if(print_status):
            status_log(wordTupleIndex, words_frequency_tuples_count, "Filtering vocabulary by words ocurrence probability",increment_percentaje=20,memory_warnings=False)

    return new_tokenized_text

def filter_tokenized_text_with_words_frequency_filter(tokenized_text, sample_rate = 0.001, print_status = False, status_verbose = 0):

    """
    Función que aplica un filtro de conservación a un texto tokenizado a partir de la probabilidad de conservación
    calculada en base a la frecuencia de ocurrencia de las palabras en el mismo texto. 
    El filtro consiste en generar un valor entre 0 y 1, que en el caso de ser mayor o igual a la probabilidad
    de la palabra, la palabra es conservada en el texto, caso contrario, es eliminada.

    Este proceso recibe el nombre de sub-sampling.

    Parameters
    ----------
        tokenized_text : list of str
            Texto tokenizado al cual se le quiere realizar sub-sampling

        sample_rate : float, optional
            Valor float que determina el coeficiente de cuanto afecta a la probabilidad de eliminar una palabra
            en base a su número de ocurrencias. Mientras más bajo, menor probabilidad de que una palabra sea
            conservada(Usando formula de Word2Vec). (Por defecto es 0.001)

    Returns
    -------

        new_tokenized_text : list of str
            Nuevo texto tokenizado con las palabras ya filtradas según su probabilidad de conservación.

    """

    tokenized_text_tuples = calculate_keep_probability_of_words_in_sentence(tokenized_text, sample_rate, print_status, status_verbose)
    new_tokenized_text = filter_by_words_ocurrence_probability(tokenized_text_tuples)
        
    return new_tokenized_text
    
def pick_a_random_word(tokenized_text):
    return tokenized_text[np.random.randint(0,len(tokenized_text))]
