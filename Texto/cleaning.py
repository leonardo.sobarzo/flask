from Texto.debugging_insights import get_process_status as status_log

#Author : Javier D.  

def clean_string(input_string, custom_chars=[], remove_chars = False ):
    """

    Función para limpiar un string de entrada de caracteres no deseados. La función 
    permite pasar por argumento un conjunto de caracteres en forma de string
    o lista que se deseen conservar o eliminar. 

    Parameters
    ----------

    input_string : str
        String a limpiar de caracteres indeseados

    custom_chars : list of char or str, optional
        Lista de caracteres personalizados que se utilizaran para realizar el limpiado.
        Si la lista entregada es vacía por defecto se definira como el conjunto de caracteres
        del alfabeto latino más el espacio (Por defecto es []).

    remove_chars : bool
        Booleano que indica si los caracteres personalizados son caracteres deseados 
        o indeseados (limpia o mantiene del string de entrada lo que pertenece a esta lista). 
        (por defecto es False).
    

    Returns
    -------

    output_string : str
        Palabra limpiada de caracteres indeseados.

    """
    output_string=""
    if (custom_chars == None):
        custom_chars = []

    if (len(custom_chars)!=0):
        for char in input_string:
            if (remove_chars):
                if (char not in custom_chars):
                    output_string+=(char)
            else:
                if (char in custom_chars):
                    output_string+=(char)
    else:
        valid_chars = " abcdefghijklmnñopqrstuvwxyzáéíóúABCDEFGHIJKLMNÑOPQRSTUVWXYZÁÉÍÓÚ"
        for char in input_string:
            if (char in valid_chars):
                output_string+=(char)

    return output_string

def clean_string_list(input_string_list, custom_chars=[], remove_chars = False, 
                    print_status=False, status_verbose=0):
    """

    Función para limpiar una lista de string de entrada de caracteres no deseados. La función 
    permite pasar por argumento un conjunto de caracteres en forma de string
    o lista que se deseen conservar o eliminar. 

    Parameters
    ----------
        input_string_list : list of str
            Lista de strings a limpiar de caracteres indeseados

        custom_chars : list of char, optional
            Lista de caracteres personalizados que se utilizaran para realizar el limpiado.
            Si la lista entregada es vacía por defecto se definira como el conjunto de caracteres
            del alfabeto latino más el espacio (Por defecto es []).

        remove_chars : bool
            Booleano que indica si los caracteres personalizados son caracteres deseados 
            o indeseados (limpia o mantiene del string de entrada lo que pertenece a esta lista). 
            (por defecto es False).
    
        print_status : bool, optional
            Booleano para determinar si se desea un feedback del status de procesamiento de la lista dada (por defecto False)

        status_verbose : int, optional
            Modo de impresión del feedback, verbose = 0 corresponde a un mensaje genérico de Loading estático que imprime un mensaje 
            en la primer y último ciclo, verbose > 0 imprime una barra de progreso en cada ciclo (Por defecto es 0).

    Returns
    -------
        clean_wordsList : list of str
            Palabras limpiadas de caracteres indeseados.

    """
    cleaned_strings_List=[]

    total_data = len(input_string_list)
    for string_index, string in enumerate(input_string_list):
        string = clean_string(string, custom_chars, remove_chars)
        if (string != ""):
            cleaned_strings_List.append(string)
        if (print_status):
            status_log(string_index, total_data, "Cleaning string list", status_verbose)
    return cleaned_strings_List
