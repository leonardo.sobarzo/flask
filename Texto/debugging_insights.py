from colorama import init
from colorama import Fore
import logging as log

init(autoreset=True)

#NOTA: ESTOS UTILITARIOS NECESITAN LA LIBRERIA COLORAMA (USADA EN "add_color_to_string) PARA MOSTRAR STRINGS DE COLORES

def add_color_to_string(string, color = "white"):
	"""
	Función para agregar color a un string que será impreso en consola.

	Colores disponibles:

		* "red" (Rojo).
		* "yellow" (Amarillo).
		* "green" (Verde).
		* "cyan" (Cyan).
		* "magenta" (Magenta).
		* "blue" (Blue).
		* "white" (Blanco).

	Parameters
	----------
	string : str
		String al que se le desea agregar color.

	color : int
		Color que se le desea agregar a un string (Por defecto es "white").

	Returns
	----------

	colored_str : str
		String coloreado.

	"""
	color.lower()
	colored_str = string

	if (color == "red"):
		colored_str =  Fore.RED + string
	elif (color == "yellow"):
		colored_str =  Fore.YELLOW + string
	elif (color == "green"):
		colored_str =  Fore.GREEN + string
	elif (color == "cyan"):
		colored_str =  Fore.CYAN + string
	elif (color == "magenta"):
		colored_str =  Fore.LIGHTMAGENTA_EX + string
	elif (color == "blue"):
		colored_str =  Fore.BLUE + string
	elif (color == "white"):
		colored_str =  Fore.WHITE + string
	elif (color == "orange"):
		orange_color_code = "208" 
		orange_color = u"\u001b[38;5;" + orange_color_code + "m"
		colored_str =  orange_color + string
	elif (color == "light green"):
		ligh_green_color_code = "192" 
		light_green_color = u"\u001b[38;5;" + ligh_green_color_code + "m"
		colored_str =  light_green_color + string


	return colored_str

def get_process_status(p_index, total_data, process_name = "", increment_percentage = 1, memory_warnings = True):
	
	"""
	Función para imprimir en consola el estado de procesamiento de una función o algoritmo basado en un loop a
	partir de un número total de datos y un contador. La función además previene colgamientos de sistema.

	Parameters
	----------

	p_index : int
		Indice de progreso, es el contador o número que indica el número vuelta o ciclo del loop.
	
	total_data : int
		La cantidad total de vueltas o ciclos que tendra el loop.

	process_name :  str, optional
		Nombre o identificador del proceso que se imprimira en el feedback (Por defecto es "").
	"""
	#warning_ratio = (total_data // 10) + 1
	warning_ratio = 1
	elements_per_increment = total_data // (100//increment_percentage)

	if (elements_per_increment == 0):
		elements_per_increment = 1
		increment_percentage= (elements_per_increment // total_data)*100
	increment_n =  p_index//elements_per_increment
	progress = increment_percentage * increment_n

	if(p_index%elements_per_increment==0):
		log.debug("Running, {} of {} - {}%...".format(p_index + 1, total_data, progress), extra={"context" : process_name})
		if (p_index==total_data-1):
			log.debug("Running, {} of {} - {}%...".format(p_index + 1, total_data, "100"), extra={"context" : process_name})

	if (memory_warnings):
		if (p_index % warning_ratio == 0):
			free_memory = get_memory_avaible()
			if (free_memory<700):
				log.warning ("[RUNNING PROCESS WARNING] JUST " + str(free_memory) + "Mb OF FREE MEMORY REMAINING !!!", extra={"context" : process_name})

			if (free_memory<400):
				log.critical ("[RUNNING PROCESS EXCEPTION] JUST " + str(free_memory) + "Mb OF FREE MEMORY REMAINING, STOPING PROCESS TO AVOID SYSTEM HANGING !!!", extra={"context" : process_name})
				raise MemoryError()

def get_memory_avaible():
	"""
	Función para obtener la cantidad de memoria libre aproximada en Mb.
	"""
	with open('/proc/meminfo', 'r') as mem:
		#ret = {}
		tmp = 0
		for i in mem:
			sline = i.split()
			if str(sline[0]) == 'MemTotal:':
				#ret['total'] = int(sline[1])
				total_memory = int(sline[1])
			elif str(sline[0]) in ('MemFree:', 'Buffers:', 'Cached:'):
				tmp += int(sline[1])

	#ret['free'] = tmp

	#ret['used'] = int(ret['total']) - int(ret['free'])
	free_memory = tmp
	#used_memory = total_memory - free_memory
	#free_memory_ratio = free_memory / total_memory

	free_memory_mb = free_memory // 1024

	return free_memory_mb
