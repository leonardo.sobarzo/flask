#!/usr/bin/env python
import json
import numpy as np
import os
import pickle
import sys
import tensorflow as tf
from keras.models import model_from_json
from tensorflow import keras
from tensorflow.keras.metrics import Precision, Recall

from Texto.embedding import WordVectors
from Texto.Amazon_Querys import  getComidas, putComidas


#Al hacer from train import F1... importa también el requisito de parámetro de entrenamiento -c
class F1_Score(tf.keras.metrics.Metric):

    def __init__(self, name='f1_score', **kwargs):
        super().__init__(name=name, **kwargs)
        self.f1 = self.add_weight(name='f1', initializer='zeros')
        self.precision_fn = Precision(thresholds=0.5)
        self.recall_fn = Recall(thresholds=0.5)

    def update_state(self, y_true, y_pred, sample_weight=None):
        p = self.precision_fn(y_true, y_pred)
        r = self.recall_fn(y_true, y_pred)
        # since f1 is a variable, we use assign
        self.f1.assign(2 * ((p * r) / (p + r + 1e-6)))

    def result(self):
        return self.f1

    def reset_states(self):
        # we also need to reset the state of the precision and recall objects
        self.precision_fn.reset_states()
        self.recall_fn.reset_states()
        self.f1.assign(0)


def leer_comida(datos):
    #desayuno
    #once
    #almuerzo
    #extra

    desayuno=datos["desayuno"]["S"].split(" ")
    desayuno=desayuno + (50-len(desayuno))*[" "]

    almuerzo=datos["almuerzo"]["S"].split(" ")
    almuerzo=almuerzo + (50-len(almuerzo))*[" "]

    once=datos["once"]["S"].split(" ")
    once=once + (50-len(once))*[" "]

    extra=datos["extra"]["S"].split(" ")
    extra=extra + (50-len(extra))*[" "]
    return [desayuno,almuerzo,once,extra] 


def predecirTexto(datos,fecha):
    data_json = json.load(open("/home/ubuntu/Flask/Texto/conf_prediccion.json", "r")) #-> aun con un conf habria que modificar esto
    ruta_modelo = data_json["ruta_modelos"]
    embedding_path = data_json["ruta_embedding"]

    modelo_escogido = data_json["modelo"]
    ruta = ruta_modelo + "/" + modelo_escogido + "/"

    #Cargar archivos, se utiliza los mismos nombres que en train.py
    archivos_carpeta = os.listdir(ruta)
    archivos_carpeta.sort()
    
    objeto = model_json = model_weights = None

    with open(ruta + archivos_carpeta[1], 'rb' ) as archivo_guardado:
        print("[ INFO ] Cargando _params.pkl")
        objeto = pickle.load(archivo_guardado)

    with open(ruta + archivos_carpeta[0], 'r') as json_file:
        print("[ INFO ] Cargando _architecture.json")
        model_json = json_file.read()

#Cargar modelo
    model = model_from_json(model_json)
#model.summary()
    model.load_weights(ruta + archivos_carpeta[2])
    model.compile(optimizer="adam", loss = "categorical_crossentropy", metrics = ["accuracy", F1_Score()])

#Obtener datos y hacerle el embedding
    a_predecir = leer_comida(datos)
    
    ft_model = WordVectors(embedding_path)
    embedding_sentence_list = ft_model.sentence_list_predict(a_predecir) #-> poner el ejemplo parece

#Correr el modelo
    prediccion = model.predict(embedding_sentence_list)
    labels_predichos = []
    for j in range(4):
        labels_predichos.append([])
        for i in range(50):
            labels_predichos[j].append(np.argmax(prediccion[j][i]))
    #print(labels_predichos)

#Revisar respuesta
    labels = []
    with open(ruta + "tags.txt", "r") as labels_file:
        labels = labels_file.read()
        labels = labels.split(";")

    dictio = []

    for num,comida in enumerate(labels_predichos):
        dictio.append({})
        for index,etiqueta in enumerate(comida):
            if  labels[int(etiqueta)] == 'O':
                continue
            if labels[int(etiqueta)] not in dictio[num]:
                dictio[num][labels[int(etiqueta)]] = [a_predecir[num][index]]
            else:
                dictio[num][labels[int(etiqueta)]].append(a_predecir[num][index])
 

    #print(labels[int(etiqueta)], end=" ")
    json_labels=[]
    for comida in dictio:
        json_labels.append(json.dumps(comida))

    return json_labels

def nlp(UserID, Fecha):
    Comidas = getComidas(UserID,Fecha)
    if Comidas == None:
        return
    resultado = predecirTexto(Comidas, Fecha)
    putComidas(resultado,UserID,Fecha)
    print("[ INFO ] Resultado NLP: ", resultado) 
