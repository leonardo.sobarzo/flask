from argparse import ArgumentParser
from multiprocessing import Pool
import torchaudio
import os
import numpy as np

from NISP.dataset import NISPDataset
from NISP.lightning_model import LightningModel
import wavencoder

import pytorch_lightning as pl

from config import NISPConfig
import pandas as pd
import torch
import torch.utils.data as data

# Preparar preprocesamiento del audio
wav_len = 8000
test_transform = wavencoder.transforms.Compose([
            wavencoder.transforms.PadCrop(pad_crop_length=wav_len)
            ])

# Lista de archivos
test_path = '/home/ubuntu/Descargas/speakerprofiling/SpeakerProfiling/Final_Folder/TEST'
files = os.listdir(test_path)

# Preparar modelo
modelCheckpoint = '/home/ubuntu/Descargas/speakerprofiling/SpeakerProfiling/DOSENUNO V2/lightning_logs/version_2/checkpoints/epoch=26-step=4130.ckpt'
model = LightningModel.load_from_checkpoint(modelCheckpoint)

# Almacenar resultados y cargar datos reales
csv_path = '/home/ubuntu/Descargas/speakerprofiling/SpeakerProfiling/NISP-Dataset/total_spkrinfo.list'
df = pd.read_csv(csv_path, sep = ' ')
resultados = [] # [(real1, predicho1), ...]
resultados_h = []
# Cargar archivo y valores reales
for file in files:
    file_path = os.path.join(test_path, file)
    id = file[:8]
    row = df[ df[ 'Speaker_ID' ] == id ]
    w = row['Weight'].item()
    h = row['Height'].item()
    # Preparar audio
    wav, _ = torchaudio.load(file_path)
    wav = test_transform(wav)

    # Predecir
    h_h, w_h = model(wav)

    w_h = w_h.detach().numpy()
    h_h = h_h.detach().numpy()
    resultados.append([w, w_h*model.w_std+model.w_mean])
    resultados_h.append([h, h_h*model.h_std+model.h_mean])

resultados = np.asarray(resultados, dtype=object)
resultados_h = np.asarray(resultados_h, dtype=object)

mape_w = np.sum(np.abs((resultados[:,0]-resultados[:,1])/resultados[:,0]))/resultados.shape[0]
mape_h = np.sum(np.abs((resultados_h[:,0]-resultados_h[:,1])/resultados_h[:,0]))/resultados_h.shape[0]


print("MAPE w: ",mape_w)
print("MAPE h: ",mape_h)
print(resultados[0:10,:])
