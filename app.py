from flask import Flask
from Amazon_Querys import GetWav
from Amazon_Querys import putItem
from datetime import date
from datetime import datetime
from random import randint
from Modelos_imc.Model import load_model, evaluate_file, calc_IMC
from Texto.ModeloText import nlp
import os
from predict import decente

app = Flask(__name__)

#if name == "main":
#    app.run(debug=True, port=4000)


@app.route('/ping', methods=['GET'])
def ping():
    return "pong!"

@app.route('/imc/<string:key_wav>&<string:key_user>&<string:fecha_comidas>', methods=['GET'])
def get_imc(key_wav, key_user,fecha_comidas):
    #obtener Wav y guardar localmente
    #   GetWav("recordings/00fc58c4-94e6-43e3-b792-865b6a442c17_2021-07-22T02:27:53.725+0000_AUDIO_FROM_CUSTOMER.wav", "Test1.wav")
    key_wav = key_wav.replace("---","/")
    nombreArchivo = key_wav.split("/")[-1]
    #print(key_wav)
    #print(key_user)
    print("[ INFO ] Nombre archivo ",nombreArchivo)
    GetWav(key_wav,nombreArchivo)
    # run model.evaluate(wav)
    ModeloAltura = load_model("Modelos_imc/Model V4H")
    ModeloPeso = load_model("Modelos_imc/Model V4W")
    Altura = round(evaluate_file(ModeloAltura,nombreArchivo),2)
    Peso = round(evaluate_file(ModeloPeso,nombreArchivo),2)

    #Segundo modelo
    peso2 = decente(nombreArchivo)

    imc = round(calc_IMC(peso2,Altura),2)
    nlp(key_user,fecha_comidas)
    os.remove(nombreArchivo)
    # Guardar en BD
    FechaAhora = datetime.now()
    Id = str(randint(1,9000000)) #hay que generar id distintos
    Date = str(FechaAhora.today())
    #Time=str(FechaAhora.hour)+":"+str(FechaAhora.minute)+":"+str(FechaAhora.second)
    #Height =str(altura)
    #Weight =str(peso)
    IMC = str(imc)
    UserID = key_user
    Altura = str(Altura)
    Peso = str(Peso)

    putItem(Id, Date, UserID, IMC, Altura, Peso)
    # return resultado del modelo
    return IMC
