from keras import backend as K
import tensorflow as tf
import numpy as np
from Modelos_imc.Audio_Pre_Processing import preprocess_dataset

def root_mean_squared_error(y_true, y_pred):
    return K.sqrt(K.mean(K.square(y_pred - y_true)))

def load_model(model_folder):
    new_model = tf.keras.models.load_model(model_folder, custom_objects={'root_mean_squared_error':root_mean_squared_error})
    return new_model

def evaluate_file(model, file_path):
    audio_file = tf.constant(file_path, dtype=tf.string)
    audio_mfccs = preprocess_dataset([audio_file])
    for audio in audio_mfccs:
        metric = model.predict(audio)[0][0]
    return metric

def calc_IMC(weight, height):
    return weight/(height/100)**2


"""
Para probar
"""
#weight_model_path = "/home/ubuntu/App_wav/Modelos_imc/Model V4W"
#height_model_path = "/home/ubuntu/App_wav/Modelos_imc/Model V4H"
#audio_file_path = "/home/ubuntu/App_wav/Modelos_imc/Audio_Sample_IMC-25.05.wav"

#weight_model = load_model(weight_model_path)
#height_model = load_model(height_model_path)

#predicted_weight = evaluate_file(weight_model, audio_file_path)
#predicted_height = evaluate_file(height_model, audio_file_path)

#predicted_IMC = calc_IMC(predicted_weight, predicted_height)

#print(predicted_IMC)
