import tensorflow as tf
import os

AUTOTUNE = tf.data.AUTOTUNE

def decode_audio(audio_binary):
    audio, _ = tf.audio.decode_wav(audio_binary, desired_samples=160000)
    return tf.squeeze(audio, axis=-1)

def get_waveform(file_path):
    audio_binary = tf.io.read_file(file_path)
    waveform = decode_audio(audio_binary)  
    return waveform

def get_mfccs(waveform):
    waveform = tf.cast(waveform, tf.float32)
    stfts = tf.signal.stft(waveform, frame_length=1024, frame_step=256,
                       fft_length=512)
    spectrograms = tf.abs(stfts)
    num_spectrogram_bins = stfts.shape[-1]

    
    lower_edge_hertz, upper_edge_hertz, num_mel_bins, sample_rate = 80.0, 4000.0, 80, 8000
    linear_to_mel_weight_matrix = tf.signal.linear_to_mel_weight_matrix(
        num_mel_bins, num_spectrogram_bins, sample_rate, lower_edge_hertz,
        upper_edge_hertz)
    
    mel_spectrograms = tf.tensordot(
        spectrograms, linear_to_mel_weight_matrix, 1)
    
    mel_spectrograms.set_shape(spectrograms.shape[:-1].concatenate(
        linear_to_mel_weight_matrix.shape[-1:]))

    log_mel_spectrograms = tf.math.log(mel_spectrograms + 1e-6)

    mfccs = tf.signal.mfccs_from_log_mel_spectrograms(
        log_mel_spectrograms)[..., :13]

    mfccs = tf.expand_dims(mfccs,0)
    mfccs = tf.expand_dims(mfccs,-1)
    return mfccs


def preprocess_dataset(files):
    files_ds = tf.data.Dataset.from_tensor_slices(files)
    output_ds = files_ds.map(get_waveform, num_parallel_calls=AUTOTUNE)
    output_ds = output_ds.map(get_mfccs,  num_parallel_calls=AUTOTUNE)
    return output_ds