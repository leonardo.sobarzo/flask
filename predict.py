from argparse import ArgumentParser
from multiprocessing import Pool
import torchaudio
import os

from NISP.dataset import NISPDataset
from NISP.lightning_model import LightningModel
import wavencoder

import pytorch_lightning as pl

from config import NISPConfig

import torch
import torch.utils.data as data

# Preparar preprocesamiento del audio

def decente(file_path):
    wav_len = 8000
    test_transform = wavencoder.transforms.Compose([
            wavencoder.transforms.PadCrop(pad_crop_length=wav_len)
            ])


# Cargar archivo
#file_path = '/home/ubuntu/Descargas/speakerprofiling/SpeakerProfiling/Final_Folder/TEST/Hin_0092_Hin_f_0000.wav'
    wav, _ = torchaudio.load(file_path)
    wav = test_transform(wav)

# Preparar modelo
    modelCheckpoint = '/home/ubuntu/flask/lightning_logs/version_2/checkpoints/epoch=26-step=4130.ckpt'
    model = LightningModel.load_from_checkpoint(modelCheckpoint)

# Predecir
    y_h, w_h = model(wav)

    w_h = w_h.detach().numpy()

    return (w_h*model.w_std+model.w_mean)[0]

print(decente("/home/ubuntu/flask/test.wav"))
